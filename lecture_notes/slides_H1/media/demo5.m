function demo3()

fh = figure;
colormap(jet);
ha = subplot('position',[.08 .16 .9 .76]);
hb1 = uicontrol('Style','pushbutton','Parent',fh,'Position',[ 10 10 90 20],'String','prev','Callback',@demo_prev); 
hb2 = uicontrol('Style','pushbutton','Parent',fh,'Position',[110 10 90 20],'String','next','Callback',@demo_next); 
hb3 = uicontrol('Style','pushbutton','Parent',fh,'Position',[210 10 90 20],'String','play','Callback',@demo_play); 
hb4 = uicontrol('Style','pushbutton','Parent',fh,'Position',[410 10 90 20],'String','stop','Callback',@demo_stop); 

demo_ndx = demo_do(1,1);


function demo_prev(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx-1,1);
end;

function demo_next(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx+1,1);
end;

function demo_play(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx,2);
end;

function demo_stop(source,event,handles,varargin)
close(fh);
end;

function ndx=demo_do(ndx,action)
ndx = min(max(ndx,1),9);

switch ndx;
case 1
[s,fs] = audioread('man.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('man -- normaal');

case 2
[s,fs] = audioread('man_fast_wrong.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('man -- versneld, foutief');

case 3
[s,fs] = audioread('man_fast.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('man -- versneld');

case 4
[s,fs] = audioread('man_slow.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('man -- vertraagd');

case 5
[s,fs] = audioread('man_gender.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('man --> vrouw');

case 6
[s,fs] = audioread('vrouw.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('vrouw -- normaal');

case 7
[s,fs] = audioread('vrouw_gender.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('vrouw --> man');

case 8
[s,fs] = audioread('vrouw_fast.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('vrouw -- versneld');

case 9
[s,fs] = audioread('vrouw_slow.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('vrouw -- vertraagd');

end	% switch

if(action == 2)
  wavplay(s,fs);
end
end

end
