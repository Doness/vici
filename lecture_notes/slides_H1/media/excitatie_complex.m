function excitatie_complex(sig, F0, Nh)
%
%	excitatiespectrum van complexe toon
%   sig = 1 ==> Nh even sterke harmonischen van F0
%       = 2 ==> tonen (F0,A1dB) en (1.35F0,A1dB - [10 20 30])
%

 fmin = 0.05;
 fmax = 20;

 logf = log10(fmin):log10(1.01):log10(fmax);
 Nf   = size(logf,2);

 f    = 10.^logf;

 left  = 40;
 right = 10;

 if (sig == 1)
   for (m=1:Nh) logA(m) = 70; Fh(m) = m*F0; end
   Nfig = 1;
 else
   Nh = 2; logA(1) = 80; Fh(1) = F0; 
           logA(2) = 75; Fh(2) = 1.35*F0;
   Nfig = 5;
 end

 clf;
 
 for (ifig=1:Nfig)
 
   EXC = zeros(Nf,Nh);
   Etot = zeros(1,Nf);
   Delta = zeros(1,Nf);
   Dmax = -10;
   
   for (n=1:Nf)
     for (m=1:Nh)
       x = logf(n) - log10(Fh(m));
% was eerder 40*exp -10, maar dat gaf bijdrage voor en achter
% wegens vlak EXC(\infty) = -10 (nu -30) 
       if (x<0) EXC(n,m) = logA(m)*exp(-left *x*x) - 30;
           else EXC(n,m) = logA(m)*exp(-right*x*x) - 30; end
       Etot(n) = Etot(n) + 10.^(EXC(n,m)/10);
     end
     if (Etot(n) > 1)
        Delta(n) = (Etot(n) - 10.^(EXC(n,1)/10))/(Etot(n) + 30);
        if (Delta(n) > Dmax) nmax = n; Dmax = Delta(n); end
     end
     Etot(n) = 10*log10(Etot(n));
   end
%  set(gca,'XTick',{log10(0.1),log10(1.0),log10(10)})
   semilogx(f,EXC(:,1),'LineWidth',1,'Color','blue');
   hold on;
   for (m=2:Nh)
     semilogx(f,EXC(:,m),'LineWidth',1,'Color','blue');
   end;
   semilogx(f,Etot,'LineWidth',2,'Color','red')
   ylim([0,50]);
   xlim([fmin,fmax]);
   set(gca,'XTickLabel',{'0.1','1.0','10.0'});
   xlabel('frequentie (kHz)');
   ylabel('excitatiespectrum (dB)');
   if (sig == 2)
      semilogx([f(nmax) f(nmax)],[EXC(nmax,1) Etot(nmax)],'.-k');
   end
   hold off;
   
   if (ifig < Nfig) waitforbuttonpress; logA(2) = logA(2) - 5; end
 end
