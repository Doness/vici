function demo3()

fh = figure;
colormap(jet);
ha = subplot('position',[.08 .16 .9 .76]);
hb1 = uicontrol('Style','pushbutton','Parent',fh,'Position',[ 10 10 90 20],'String','prev','Callback',@demo_prev); 
hb2 = uicontrol('Style','pushbutton','Parent',fh,'Position',[110 10 90 20],'String','next','Callback',@demo_next); 
hb3 = uicontrol('Style','pushbutton','Parent',fh,'Position',[210 10 90 20],'String','play','Callback',@demo_play); 
hb4 = uicontrol('Style','pushbutton','Parent',fh,'Position',[410 10 90 20],'String','stop','Callback',@demo_stop); 

[s,fs] = audioread('speech16.wav');
s = s(1500:28000);
demo_ndx = demo_do(1,1);


function demo_prev(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx-1,1);
end;

function demo_next(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx+1,1);
end;

function demo_play(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx,2);
end;

function demo_stop(source,event,handles,varargin)
close(fh);
end;

function ndx=demo_do(ndx,action)
ndx = min(max(ndx,1),10);

switch ndx;
case 1
spectrogram(s,16000,0.004,0.001,[],'hamming');ylim([0,5.5]);
title('breedband spectrum; Tw=4ms; window=hamming');

case 2
spectrogram(s,16000,0.008,0.002,[],'hamming');ylim([0,5.5]);
title('breedband spectrum; Tw=8ms; window=hamming');

case 3
spectrogram(s,16000,0.016,0.005,[],'hamming');ylim([0,5.5]);
title('smalband spectrum; Tw=16ms; window=hamming');

case 4
spectrogram(s,16000,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('smalband spectrum; Tw=32ms; window=hamming');

case 5
spectrogram(s,16000,0.064,0.01,[],'hamming');ylim([0,5.5]);
title('smalband spectrum; Tw=64ms; window=hamming');

case 6
spectrogram(s,16000,0.128,0.01,[],'hamming');ylim([0,5.5]);
title('smalband spectrum; Tw=128ms; window=hamming');

case 7
spectrogram(s,16000,0.032,0.01,[],'hamming');ylim([0,5.5]);
title('smalband spectrum; Tw=32ms; window=hamming');

case 8
spectrogram(s,16000,0.032,0.01,[],'rectwin');ylim([0,5.5]);
title('smalband spectrum; Tw=32ms; window=rectwin');

case 9
spectrogram(s,16000,0.004,0.001,[],'hamming');ylim([0,5.5]);
title('breedband spectrum; Tw=4ms; window=hamming');

case 10
spectrogram(s,16000,0.004,0.001,[],'rectwin');ylim([0,5.5]);
title('breedband spectrum; Tw=4ms; window=rectwin');

end	% switch


if(action == 2)
  wavplay(s,fs);
end
end


end




