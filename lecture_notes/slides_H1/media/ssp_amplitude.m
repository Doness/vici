function [a] = ssp_amplitude(x,fs,flength,fshift,win)
% function [a] = ssp_amplitude(x,fs,flength,fshift,win)
%
% Divide signal <x> with sample frequency <fs> (in Hz) into frames using the given frame length <flength>
% and frame shift <fshift> (values in seconds).
% Any of the default Maltab windows can be used.
% A window can be specified as a function pointer (e.g. @hamming) or as a string (e.g. 'rectwin').
% If no output argument is asked, the values are plotted.

if(nargin < 2)	fs=[];		end;	if(isempty(fs))		fs=16000;	end;
if(nargin < 3)	flength=[];	end;	if(isempty(flength))	flength=0.032;	end;
if(nargin < 4)	fshift=[];	end;	if(isempty(fshift))	fshift=0.01;	end;
if(nargin < 5)	win=[];		end;	if(isempty(win))	win=@hamming;	end;

if(ischar(win))
  win_str = win;
  win = str2func(win);
else
  win_str = func2str(win);
end

a = frame(x(:),flength,fshift,fs);
w = win(size(a,1));
w = w/sqrt(dot(w,w));
a = sqrt(sum(bsxfun(@times,a,w).^2));
if(nargout == 0)
  subplot('position',[0.08 0.61 0.9 .31]);plot((1:numel(x))*(1000/fs),x(:));xlim([0,numel(x)*(1000/fs)]);xlabel('tijd in ms');
  title(sprintf('signaal + korte-termijn amplitude (Tw=%.1fms, win=%s)',flength*1000,win_str));
  subplot('position',[0.08 0.18 0.9 .31]);plot((flength/2+(1:numel(a))*fshift)*1000,a);xlim([0,numel(x)*(1000/fs)]);ylim([0,ceil(max(a)/0.25)*0.25]);xlabel('tijd in ms');
  drawnow;
end
