function demo1()

if(ismac())
  set(0,'DefaultAxesFontSize',14);
  set(0,'DefaultTextFontSize',14);
elseif(isunix())
  set(0,'DefaultAxesFontSize',9);
  set(0,'DefaultTextFontSize',9);
end
fh = figure;
hb1 = uicontrol('Style','pushbutton','Parent',fh,'Position',[ 10 10 90 20],'String','prev','Callback',@demo_prev); 
hb2 = uicontrol('Style','pushbutton','Parent',fh,'Position',[110 10 90 20],'String','next','Callback',@demo_next); 
hb3 = uicontrol('Style','pushbutton','Parent',fh,'Position',[210 10 90 20],'String','play','Callback',@demo_play); 
hb4 = uicontrol('Style','pushbutton','Parent',fh,'Position',[410 10 90 20],'String','stop','Callback',@demo_stop); 
[sl,fs] = audioread('goelies_16k.wav');
ss = sl(9000:21000);
demo_ndx = demo_do(1,1);


function demo_prev(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx-1,1);
end;

function demo_next(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx+1,1);
end;

function demo_play(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx,2);
end;

function demo_stop(source,event,handles,varargin)
close(fh);
end;

function ndx=demo_do(ndx,action)
ndx = min(max(ndx,1),9);

switch ndx;
case 1
s = sl;
ssp_amplitude(s,fs,0.070,0.002,'hamming');

case 2
s = ss;
ssp_amplitude(s,fs,0.010,0.002,'rectwin');

case 3
s = ss;
ssp_amplitude(s,fs,0.010,0.002,'hamming');

case 4
s = ss;
ssp_amplitude(s,fs,0.030,0.002,'hamming');

case 5
s = ss;
ssp_amplitude(s,fs,0.070,0.002,'hamming');

case 6
s = ss;
ssp_amplitude(s,fs,0.030,0.002,'rectwin');

case 7
s = ss;
ssp_amplitude(s,fs,0.070,0.002,'rectwin');

case 8
s = sl;
ssp_amplitude(s,fs,0.010,0.002,'hamming');

case 9
s = sl;
ssp_amplitude(s,fs,0.070,0.002,'hamming');

end	% switch


if(action == 2)
  wavplay(s,fs);
end
end


end
