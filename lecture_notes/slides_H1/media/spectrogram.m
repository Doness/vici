function [y,x] = spectrogram(x,sfreq,flength,fshift,sig_range,window,type,preemph,tlimits)
% function spectrogram(x,sfreq,flength,fshift,sig_range,window,type,preemph,tlimits)
%	Calculates the spectrogram for the audio signal in vector x.
%	The <tlimits> vector can be used to select a part of the signal
%       (tlimits(1):tlimits(2)). All timings are given in seconds.
% Algorithm:
%	First x is being preemphasized : z(i) = x(i) - preemph*x(i-1).
%	Then z is split in overlapping frames (flength, fshift).
%	For each frame the spectrum is calculated:
%	  spec(frame_nr) = abs(fft(X(begin_frame:end_frame).*window(nfft)));
%	Next the frequency content is calculated. Following methods can be
%       choosen (input parameter <type>):
%	  'fft':	small-band or wide-band fft-spectrogram
%	  'cep<N>':	cepstral smoothed fft-spectrogram (rect. window, N coeff.)
%	  'lpc<N>':	lpc based spectrum, N coefficients
%       <N> is the number of cepstral coefficients (type='cep'), or the order
%       of the  linear prediction filter (type='lpc').
%       Finaly, the resulting log spectrum (log(spectrum+sqrt(nfft)/3)) is
%       displayed, with sqrt(nfft)/3 the expected quantisation noise in the 
%       spectrum for linear quantizized signals. (error 1/2 digit).
%       The <sig_range> parameter can be set to brighten/darken the plot
%       (hide noise, ...). The value of <sig_range> is [min_range max_range]
%       with <min_range> and <max_range> normalized values between 0 and 1.
%
% Default values:
%	sfreq:		8000 Hz			flength:	0.032 sec
%	fshift:		0.010 sec		sig_range:	[0 1]
%	type:		'fft'			window:		'hamming'
%	preemph: 	.95			tlimits:	[0 -1]
%

% Copyright (C) 2013-2014 UGent
% Author: Kris Demuynck

if(nargin == 0)
help spectrogram
return
end
if(nargin < 2);			sfreq=[];		end;
if(isempty(sfreq));		sfreq=8000;		end;
if(nargin < 3);			flength=[];		end;
if(isempty(flength));		flength=0.032;		end;
if(nargin < 4);			fshift=[];		end;
if(isempty(fshift));		fshift=0.01;		end;
if(nargin < 5);			sig_range=[];		end;
if(isempty(sig_range));		sig_range=[0 1];	end;
if(nargin < 6);			window=[];		end;
if(isempty(window));		window='hamming';	end;
if(nargin < 7);			type=[];		end;
if(isempty(type));		type='fft';		end;
if(nargin < 8);			preemph=[];		end;
if(isempty(preemph));		preemph=.95;		end;
if(nargin < 9);			tlimits=[];		end;
if(isempty(tlimits));		tlimits = [0;-1];	end;

nfft = floor(flength*sfreq+0.5);
% Change the following line to change the window used.
if(ischar(window))
  window = str2func(window);
end
win = window(nfft);
win = win/sqrt(dot(win,win));
if(~strncmp(type,'lpc',3))
nfft2 = 1;
while(nfft2*1.01 < nfft)
  nfft2 = nfft2 * 2;
end
if(nfft2 ~= nfft)
  if(nfft2 < nfft)
    flength = nfft2/sfreq;
    fprintf(2,'flength set to %.6f\n',flength);
    win = window(nfft2);
  else
    nx  = floor((nfft2-nfft)/2);
    win = [zeros(nx,1); win ; zeros(nfft2-nfft-nx,1)];
  end
  nfft = nfft2;
end
end
nshift = floor(fshift*sfreq+0.5);
Nfft   = nfft;

% select the correct part of the data
nx = numel(x);
start = round(tlimits(1)*sfreq)-(Nfft-nshift)/2;
if(start < 0)	start = 0;	end;
if(start > nx)	start = nx;	end;
if(tlimits(2) == -1)
  stop = nx;
else
  stop = round(tlimits(2)*sfreq)+(nshift-1)+(Nfft-nshift)/2;
end
if(stop < 0)	stop = 0;	end;
if(stop > nx)	stop = nx;	end;

% make a column vector
x = x(start+1:stop);
nx = length(x);
% preemphasize if requested.
if(max(abs(x)) <= 1)		% audioread scales the signal to the range [-1:1]
  x = x*32768;
end
if(preemph ~= 0)
  x(:) = x - preemph*[x(1) ; x(1:nx-1)];
end

% assumptions: x = integer, quantisation only correct up to 1/2 digit
noise_thr = 1/3;

% figure out number of columns for offsetting the signal
% this may truncate the last portion of the signal since we'd
% rather not append zeros unnecessarily - also makes the fancy
% indexing that follows more difficult.
ncol = 1+floor((nx-nfft)/nshift);

% now stuff x into columns of y with the proper offset
y = reshape(x(bsxfun(@plus,(1:nfft)',(0:(ncol-1))*nshift)),nfft,ncol);
clear x;

% Apply the window to the array of offset signal segments.
y(:) = bsxfun(@times,y,win);

half = 1;
if(~strcmp(type,'fft'))
ncoeff = str2num(type(4:end));
if (ncoeff <= 0)
  error('ncoeff should be larger than 0');
end
if(strncmp(type,'cep',3))
  y(:) = ifft(20*log10(abs(fft(y))+noise_thr));
  y(ncoeff+1:nfft-ncoeff+1,:) = zeros(nfft-2*ncoeff+1,ncol);
  y(:) = real(fft(y));
elseif(strncmp(type,'lpc',3))
  [x,gain] = levinson(real(ifft(abs(fft(y,2^nextpow2(2*size(y,1)-1))).^2)),ncoeff);
  gain = sqrt(gain(:)');
  if (nfft < 256)
    nfft = 256;
    y    = zeros(nfft,ncol);
  end
  win = exp(pi*i*(0:nfft-1)/nfft)';
  for ii=1:ncol
    y(:,ii) = -20*log10(eps+abs(polyval(x(ii,:),win))/gain(ii));
  end
  half = 0;
else
  fprintf(2,'Unknow analysis method <%s> requested\n',type);
end
else
y(:) = 20*log10(abs(fft(y))+noise_thr);
end

% Interpolate to max(256,nfft) points (smoother figure).
if(nfft < 256)
  y(:) = real(fft(y));
  y = [y(1:nfft/2+1,:) ; zeros(256-nfft-1,ncol) ; y(nfft/2+1:nfft,:)];
  y = real(ifft(y));
  nfft = 256;
end 

% Display only the half of the spectrogram (symmetric) (nfft/2 points).
if(half)
  y(nfft,:)       = .5*(y(nfft/2,:) + y(nfft/2+1,:));
  y(1:2:nfft-1,:) = y(1:nfft/2,:);
  y(2:2:nfft-2,:) = (y(1:2:nfft-3,:) + y(3:2:nfft-1,:)) * .5;
end

% show_spec(y,sig_range,fshift,sfreq,Nfft);
ncol  = size(y,2);
ymin  = min(min(y));
ymax  = max(max(y));
range = ymax-ymin;
if(max(size(sig_range)) == 3)
sig_range = (sig_range-ymin)/range;
end
offset = ymin + range*sig_range(1);
range  = range*(sig_range(2)-sig_range(1));
y = min(max(y,offset),offset+range);
if(nargout == 0)
  h = imagesc([nfft*.5 ; nfft*.5+(ncol-1)*nshift]/sfreq,[0 ; sfreq/2000],y);axis xy;
  %colorbar;
  set(h,'UserData',[offset offset+range]);
  xlabel('tijd (sec)');
  ylabel('frequentie (kHz)');
end
return;
