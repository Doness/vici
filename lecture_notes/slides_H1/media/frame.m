function y = frame(x,flength,fshift,sample_freq,preemph)
% Usage: y = frame(x,flength,fshift,sample_freq,preemph)
%
% Make overlapping frames + apply a Hamming window (no preemphasis by default).
%
% Default values:
%       flength:        0.03 sec        fshift:         0.01 sec        
%       sample_freq:    16 kHz          preemph:        0.0

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

if(nargin < 5)		preemph = [];
  if(nargin < 4)	sample_freq = [];
    if(nargin < 3)	fshift = [];
      if(nargin < 2)	flength = [];
        if(nargin == 0)
        help frame;
        return;
end, end, end, end, end

if(isempty(preemph))	 preemph=0;		end;
if(isempty(sample_freq)) sample_freq=16000;	end;
if(isempty(fshift))	 fshift=0.01;		end;
if(isempty(flength))	 flength=0.03;		end;

nshift = floor(fshift*sample_freq+0.5);
nwin   = floor(flength*sample_freq+0.5);

% pre-emphasis
nx = numel(x);
if(preemph ~= 0)
  x(:) = x - preemph*[x(1) ; x(1:nx-1)];
end

% create overlapping frames
nfr = 1+floor((nx-nwin)/nshift);
y = reshape(x(bsxfun(@plus,(1:nwin)',(0:(nfr-1))*nshift)),nwin,nfr);
