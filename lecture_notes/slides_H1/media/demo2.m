function demo2()

fh = figure;
ha = subplot('position',[.08 .16 .9 .76]);
hb1 = uicontrol('Style','pushbutton','Parent',fh,'Position',[ 10 10 90 20],'String','prev','Callback',@demo_prev); 
hb2 = uicontrol('Style','pushbutton','Parent',fh,'Position',[110 10 90 20],'String','next','Callback',@demo_next); 
hb3 = uicontrol('Style','pushbutton','Parent',fh,'Position',[210 10 90 20],'String','play','Callback',@demo_play); 
hb4 = uicontrol('Style','pushbutton','Parent',fh,'Position',[410 10 90 20],'String','stop','Callback',@demo_stop); 
demo_ndx = demo_do(1,1);


function demo_prev(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx-1,1);
end;

function demo_next(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx+1,1);
end;

function demo_play(source,event,handles,varargin)
demo_ndx = demo_do(demo_ndx,2);
end;

function demo_stop(source,event,handles,varargin)
close(fh);
end;

function ndx=demo_do(ndx,action)
ndx = min(max(ndx,1),14);

switch ndx;
case 1
[s,fs] = audioread('sine440.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;ylim([0,5.5]);
title('sinus @ 440Hz; Tw=32ms; window=hamming');

case 2
[s,fs] = audioread('sine880.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;ylim([0,5.5]);
title('sinus @ 880Hz; Tw=32ms; window=hamming');

case 3
[s,fs] = audioread('sweep.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('freq. sweep; Tw=32ms; window=hamming');

case 4
[s,fs] = audioread('noise.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('ruis (wit+HF); Tw=32ms; window=hamming');

case 5
[s,fs] = audioread('impuls.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
s = conv(s,ones(16,1));
title('pulsen; Tw=32ms; window=hamming');

case 6
[s,fs] = audioread('pulse200.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');g=gray;colormap(g(end:-1:1,:));colorbar;
title('pulsentrein; Tw=32ms; window=hamming');

case 7
[s,fs] = audioread('FrereJacques_short.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');ylim([0,5.5]);colormap(jet);colorbar;
title('piano; Tw=32ms; window=hamming');

case 8
[s,fs] = audioread('drum_short.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('drum; Tw=32ms; window=hamming');

case 9
[s,fs] = audioread('percussion_short.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('percussion; Tw=32ms; window=hamming');

case 10
[s,fs] = audioread('music_short.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('music; Tw=32ms; window=hamming');

case 11
[s,fs] = audioread('sweep.wav');
s = (s(1:2:end)+s(2:2:end))/2;
fs = fs/2;
%audiowrite('sweep_aliasing.wav',s,fs);
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('sweep+aliasing; Tw=32ms; window=hamming');

case 12
[s,fs] = audioread('goelies_16k.wav');
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('spraak; Tw=32ms; window=hamming');

case 13
[s,fs] = audioread('goelies_16k.wav');
s = max(min(s,.15),-.15);
%audiowrite('goelies_16k_clipping.wav',s,fs);
spectrogram(s,fs,0.032,0.01,[],'hamming');colormap(jet);colorbar;
title('spraak+clipping; Tw=32ms; window=hamming');

case 14
[s,fs] = audioread('goelies_16k.wav');
s = max(min(s,.15),-.15);
%audiowrite('goelies_16k_clipping.wav',s,fs);
plot((1:numel(s))/fs,s);xlabel('tijd (s)');ylim([-.2,.2]);ylabel('amplitude');colorbar;
title('spraak+clipping; Tw=32ms; window=hamming');
end	% switch


if(action == 2)
  wavplay(s,fs);
end
end


end



