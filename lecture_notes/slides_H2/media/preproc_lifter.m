function y=preproc_lifter(preproc,x)
% function y=preproc_lifter(preproc,x)
%
% Apply the liftering (element-wise scaling of the feature vector).
% Only needed for DTW!

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

y = bsxfun(@times,x,preproc.lift);
return;
