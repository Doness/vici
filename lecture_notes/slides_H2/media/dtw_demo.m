function dtw_demo(action,wsel)
%
%Interactive training and testing of the dtw recognition-engine.
%
%Load:  Load a speech-signal and isolate the words by means of silence/speech
%       detection. A word can be selected by clicking on the desired word in
%       the waveform plot.
%       The words loaded in the upper section (train data) make up the DTW-
%       templates (the reference words).
%       The words loaded in the lower section (test data) can be used as test
%       data for the recognizer.
%Play:  Play back the selected word.
%Plot:  Show the LPC-cepstral parameters used for the recognition (in the form
%       of a spectrogram).
%Recog: Recognize the word selected in the test data. The recognized word, i.e.
%	the best matching word from the train data will be selected and
%	highlighted. The corresponding score (distance) will be displayed in
%	the button at the bottom-left (D=...).
%D=...: Show a visual represenation of the allignment of the selected word from
%       the test data with the selected word from the train data. The score is
%       output to the button (D=...).
%Info:  Show this text.
%Quit:  End the DTW-demo.

% Copyright (C) 2014 UGent
% Author: Kris Demuynck
if(nargin < 1)
  pos = get(0,'ScreenSize');
  dtw_fig = figure('Name','DTW-demo','NumberTitle','off','MenuBar','none','Position',[max(ceil((pos(3)-850)/2),1) , max(ceil((pos(4)-8.0)/2),1) , min(pos(3),850) , min(pos(4),720)]);

  % the train samples
  uicontrol('Style','text','Units','normalized','Position',[0.01 0.58 0.06 0.41],'BackgroundColor',[.2 .2 .4],'ForegroundColor',[1 1 1],'String',(' Train data')','FontSize',10,'FontWeight','bold');
  wrd_trn = axes('Units','normalized','Position',[0.09 0.60 0.73 0.39],'YTick',[]);
  % select an individual word in the train samples
  lst_trn = uicontrol('Style','frame','Units','normalized','Position',[0.01 0.51 0.82 0.06],'BackgroundColor',[.5 .5 .5],'UserData',{'word1','word2'});
  sel_trn = uicontrol('Style','pushbutton','Units','normalized','Position',[0.02 0.52 0.10 0.04],'String','???','HorizontalAlignment','Center','Userdata',-1);
  % load / play / train
  uicontrol('Style','frame','Units','normalized','Position',[0.85 0.51 0.14 0.48],'BackgroundColor',[.5 .5 .5]);
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.94 0.12 0.04],'String','Load','Callback','dtw_demo(''load'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.88 0.12 0.04],'String','Play','Callback','dtw_demo(''play'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.82 0.12 0.04],'String','Plot','Callback','dtw_demo(''plot'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.58 0.12 0.04],'String','Info','Callback','dtw_demo(''info'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.52 0.12 0.04],'String','Quit','Callback','dtw_demo(''quit'')');
  set_udata(dtw_fig,[wrd_trn sel_trn lst_trn dtw_fig 0]);

  % the test samples
  uicontrol('Style','text','Units','normalized','Position',[0.01 0.08 0.06 0.41],'BackgroundColor',[.2 .2 .4],'ForegroundColor',[1 1 1],'String',(' Test data')','FontSize',12,'FontWeight','bold');
  wrd_tst = axes('Units','normalized','Position',[0.09 0.10 0.73 0.39],'YTick',[]);
  % select an individual word in the test samples
  lst_tst = uicontrol('Style','frame','Units','normalized','Position',[0.01 0.01 0.82 0.06],'BackgroundColor',[.5 .5 .5],'UserData',{'word1','word2'});
  sel_tst = uicontrol('Style','pushbutton','Units','normalized','Position',[0.02 0.02 0.10 0.04],'String','???','HorizontalAlignment','Center','Userdata',-1);
  % load / play / test
  uicontrol('Style','frame','Units','normalized','Position',[0.85 0.01 0.14 0.48],'BackgroundColor',[.5 .5 .5]);
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.44 0.12 0.04],'String','Load','Callback','dtw_demo(''load'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.38 0.12 0.04],'String','Play','Callback','dtw_demo(''play'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.32 0.12 0.04],'String','Plot','Callback','dtw_demo(''plot'')');
  uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.08 0.12 0.04],'String','Recog','Callback','dtw_demo(''recog'')');
  score = uicontrol('Style','pushbutton','Units','normalized','Position',[0.86 0.02 0.12 0.04],'String','D=???','Callback','dtw_demo(''xplot'')');
  set_udata(dtw_fig,[wrd_tst sel_tst lst_tst dtw_fig sel_trn score wrd_trn]);
else
  % set current axis (train/test) and get the corresponding data
  args = get(gcbo,'UserData');
  ca = args(1);
  cf = get(0,'CurrentFigure');
  set(cf,'CurrentAxes',ca);
  data = get(ca,'UserData');
  if(strcmp(action,'quit'))
    close(args(4));
    return;
  elseif(strcmp(action,'info'))
    pos = get(args(4),'Position');
    apos = get(0,'ScreenSize');
    pos = [floor(min(pos(1)+pos(3)/3,apos(3)-pos(3))) , floor(max(pos(2)-pos(4)/3,1)) , pos(3) , min(pos(4),400)];
    h = figure('Name','DTW-demo, info','NumberTitle','off','MenuBar','none','Position',pos);
    h=uicontrol('Style','edit','HorizontalAlignment','left','Units','normalized','Max',10,'BackgroundColor',[1 1 1],'Position',[0 0.1 1 0.9],'String',help(mfilename));
    set(h,'FontName','Courier')
    uicontrol('Style','pushbutton','Units','normalized','Position',[0.46 .02 0.08 .05],'ForegroundColor',[0 0 0],'String','OK','Callback','close(gcf)');
    return;
  elseif(strcmp(action,'xpos'))
    pos  = get(cf,'CurrentPoint');
    fpos = get(cf,'Position');
    apos = get(ca,'Position');
    rpos = length(data.trk)*(pos(1)/fpos(3)-apos(1))/apos(3);
    if(nargin < 2)
      wsel = -1;
      for i=1:length(data.bi)
        if((rpos>=data.bi(i)) & (rpos<=data.ei(i)))
          wsel = i;
        end
      end
    end
    pos = get(args(2),'Position');
    if(wsel == -1)
      pos(1) = 0.03;
      set(args(2),'Position',pos,'String','???','Userdata',-1,'BackgroundColor',[.75 .75 .75]);
    else
      pos(1) = apos(1)+apos(3)*0.5*(data.bi(wsel)+data.ei(wsel))/length(data.trk)-0.05;
      wlst = get(args(3),'Userdata');
      set(args(2),'Position',pos,'String',char(wlst(wsel)),'Userdata',wsel,'BackgroundColor',[.75 .75 .75]);
    end
    return;
  end
  wsel = get(args(2),'UserData');
  if(strcmp(action,'load'))
    cwd = pwd;
    if(~isempty(data))	cd(data.pathname);	end;
    [fname,pathname] = uigetfile('*.wav','--Speech_File--');
    cd(cwd);
    if(~isstr(fname))	return;			end;
    data.args = args;
    data.pathname = pathname;
    [data.wav,data.fs] = audioread([pathname '/' fname],'native');
    data.wav = double(data.wav(:,1));
    ll = size(data.wav,1);
    % compute Mel-spectra; do spectral subtraction; compute A-wgt energy; do sil/spch detection (simple)
    [y,ssp] = do_ssp(data,data.wav);
    data.trk = y;
    data.ssp = ssp;
    y = sqrt(exp(ssp.idct_inv'*y(1:end/3,:))');
    % specsub
    ys = sort(y);
    y = max(bsxfun(@times,bsxfun(@minus,y,2*ys(ceil(end*.2),:)),1./ys(ceil(end*.85),:)),0);
    % click removal
    t = y(1:end-4,:);
    for i=2:5
      t = min(t,y(i:end-5+i,:));
    end
    y(3:end-2,:) = t;
    % speech evidence per frame
    y = (y./(1+y))*(.1+sin((0:size(y,2)-1)'*(pi/(size(y,2)-1))));
    y = cumsum((y-min(y))/max(y));
    y = (y(31:end)-y(1:end-30)>.5);
    y = y.*(1-cumprod(double(y)));	% can't start with speech
    y = y(2:end)-y(1:end-1);
    data.bi = min(1+(find(y==1)+15),size(data.trk,2));
    data.ei = min((find(y==-1)+25),size(data.trk,2));
    while(data.bi(1) > data.ei(1))
      data.ei = data.ei(2:end);
    end
    nword = length(data.bi);
    wlst = cell(nword,1);
    cla;hold('on');
    for i=1:nword
      h = patch([data.bi(i) data.ei(i) data.ei(i) data.bi(i)]*ssp.fshift,[-1.08 -1.08 1.08 1.08],[.2 .3 .6]);
      set(h,'ButtonDownFcn','dtw_demo(''xpos'')','UserData',args);
      wlst{i} = sprintf('word%i',i);
    end
    set(args(3),'Userdata',wlst);
    h = plot([0:length(data.wav)-1]/data.fs,data.wav/max(abs(data.wav)),'g');
    set(h,'ButtonDownFcn','dtw_demo(''xpos'')','UserData',args);
    axis([0 ll/data.fs -1.1 1.1]);
    hold('off');
    set(ca,'UserData',data);
    % training
    if(args(5) == 0)
      global dtw_cfg;
      dtw_cfg = [];
      for i=1:nword
        dtw_demo('xpos',i);drawnow;
        dtw_demo('train',i);
      end
    end
    pos = get(args(2),'Position');
    pos(1) = 0.02;
    set(args(2),'Position',pos,'String','???','Userdata',-1);
  else
    % see if there is train / test data
    if(isempty(data))
      error_win('no speech signal loaded',args(4));
      return;
    end
    if(wsel == -1)
      error_win('no word selected',args(4));
      return;
    end
    if(strcmp(action,'play'))
      N = floor(data.ssp.fshift*data.fs+0.5);
      wavplay(data.wav(data.bi(wsel)*N+1:data.ei(wsel)*N),data.fs);
    else
      % preprocess the data
      y = data.trk(:,data.bi(wsel)+1:data.ei(wsel)+1);
      ssp = data.ssp;
      if(strcmp(action,'recog'))
        [w,ndx,s] = dtw_recog(lifter(y,ssp));
        fpos = get(cf,'Position');
        apos = get(ca,'Position');
        pos  = get(args(5),'Position');
        data = get(args(7),'UserData');
        pos(1) = apos(1)+apos(3)*0.5*(data.bi(ndx)+data.ei(ndx))/length(data.trk)-0.05;
        set(args(5),'Position',pos,'String',w,'Userdata',ndx,'BackgroundColor',[1 1 0]);
        set(args(6),'String',sprintf('D=%.2f',s(ndx)/(data.ei(wsel)-data.bi(wsel)+1)));
      elseif(strcmp(action,'train'))
        wlst = get(args(3),'Userdata');
        dtw_train(lifter(y,ssp),char(wlst(wsel)));
      elseif(strcmp(action,'plot'))
        pos = get(args(4),'Position');
        apos = get(0,'ScreenSize');
        pos(1) = floor(min(pos(1)+pos(3)/3,apos(3)-pos(3)));
        pos(2) = floor(max(pos(2)-pos(4)/3,1));
        figure('Name','LPC-spectrum (cepstra)','NumberTitle','off','MenuBar','none','Position',pos);
        axes('Units','normalized','Position',[0.07 0.15 0.90 0.80]);
        show_cep(ssp,y);
        uicontrol('Style','pushbutton','Units','normalized','Position',[0.45 .01 0.1 .05],'ForegroundColor',[0 0 0],'String','OK','Callback','close(gcf)');
      elseif(strcmp(action,'xplot'))
        pos = get(args(4),'Position');
        apos = get(0,'ScreenSize');
        pos(1) = floor(min(pos(1)+pos(3)/3,apos(3)-pos(3)));
        pos(2) = floor(max(pos(2)-pos(4)/3,1));
        figure('Name','DTW-alignment','NumberTitle','off','MenuBar','none','Position',pos);
        global dtw_cfg;
        ndx = get(args(5),'Userdata');
        if(ndx == -1)
          error_win('recognition failed!',args(4));
          return;
        end
        x = dtw_cfg.voc(ndx).tpl;
        lx = size(x,2);
        ly = size(y,2);
        ss = .6/max(lx,ly);
        h = axes('Units','normalized','Position',[0.37 0.10 ly*ss 0.25]);
        ncol=64;colormap([jet(ncol);gray(ncol)]);
        c=show_cep(data.ssp,y);mn=min(c(:));mx=max(c(:));
        sv=(ncol-.01)/(mx-mn+1e-99);image(floor((1-mn*sv)+c*sv));axis('xy');
        set(h,'Visible','off');
        h = axes('Units','normalized','Position',[0.12 0.35 0.25 lx*ss]);
        c=show_cep(data.ssp,x);mn=min(c(:));mx=max(c(:));
        sv=(ncol-.01)/(mx-mn+1e-99);image(flipud(floor((1-mn*sv)+c*sv))');axis('xy')
        set(h,'Visible','off');
        h = axes('Units','normalized','Position',[0.37 0.35 ly*ss lx*ss]);
        y = lifter(y,ssp);
        [s,c,p] = dtw(x,y);
        set(args(6),'String',sprintf('D=%.2f',s/lx));
        mn=min(c(:));mx=max(c(:));
        mxp=max(c([p+(0:ly-1)'*lx;max(1,p-1)+(0:ly-1)'*lx;min(lx,p+1)+(0:ly-1)'*lx]));
        sv=ncol/(1e-99+min(.5*(mx-mn),2*(mxp-mn)));
        d = bsxfun(@minus,reshape(x,[],lx,1),reshape(y,[],1,ly)); d = squeeze(dot(d,d));
        image(max(ncol+1,min(2*ncol,floor((ncol+1-mn*sv)+d*sv))));axis('xy');
        hold('on');plot(p,'r-*','MarkerSize',5);hold('off');
        set(h,'Visible','off');
        uicontrol('Style','pushbutton','Units','normalized','Position',[0.05 .01 0.3 .05],'ForegroundColor',[0 0 0],'String',sprintf('D=%.2f',s/lx));
        uicontrol('Style','pushbutton','Units','normalized','Position',[0.45 .01 0.1 .05],'ForegroundColor',[0 0 0],'String','OK','Callback','close(gcf)');
      end
    end
  end
end
return;

function error_win(msg,master_win)
pos = get(master_win,'Position');
pos = [pos(1)+(pos(3)-300)/2 pos(2)+(pos(4)-60)/2 300 60];
h = figure('Name','Error','NumberTitle','off','Color',[1 0 0],'MenuBar','none','Position',pos);
uicontrol('Style','text','Units','normalized','Position',[0 .40 1 .5],'BackgroundColor',[1 0 0],'ForegroundColor',[0 0 0],'String',msg,'FontSize',13,'FontWeight','bold');
uicontrol('Style','pushbutton','Units','normalized','Position',[0.45 .1 0.1 .4],'BackgroundColor',[1 0 0],'ForegroundColor',[0 0 0],'String','OK','FontSize',13,'FontWeight','bold','Callback','close(gcf)');
return;

function set_udata(parent,info)
h = get(parent,'Children');
for i=1:length(h)
  if(strcmp(get(h(i),'Type'),'uicontrol') & ~isempty(get(h(i),'Callback')) & isempty(get(h(i),'UserData')))
    set(h(i),'UserData',info);
  end
end
return;

function [y,ssp] = do_ssp(data,wav)
ssp = sprintf('ssp_%iHz',data.fs);
if(~isfield(data,ssp))
  ssp = struct('flength',0.032,'fshift',0.01,'fs',data.fs,'Ncep',12);
  K = [floor(ssp.fs*ssp.flength+0.5),2^ceil(log2(ssp.fs*ssp.flength))];
  ssp.win = [hamming(K(1));zeros(K(2)-K(1),1)];
  ssp.mel_fb    = sparse(make_mel_fb(K(2),data.fs,100));
  K = size(ssp.mel_fb,2);
  ssp.idct      = cos(pi*((1:K)-0.5)'*(0:K-1)/K);
  ssp.idct_inv  = inv(ssp.idct);
  ssp.idct      = ssp.idct(:,1:ssp.Ncep+1);
  ssp.idct_inv  = ssp.idct_inv(1:ssp.Ncep+1,:);
  ssp.delta     = [2,1,0,-1,-2]/10;
  ssp.delta_pos = numel(ssp.delta);
  ssp.delta_rep = ones(1,(ssp.delta_pos-1)/2);
  K = (sin((0:ssp.Ncep)'*(pi/ssp.Ncep))+1)*.1;
  ssp.lift      = [K;K*2;K*.5];
  setfield(data,sprintf('ssp_%iHz',data.fs),ssp);
else
  ssp = data.(ssp);
end
y = ssp.idct'*log(full(ssp.mel_fb'*abs(fft(bsxfun(@times,frame(wav,ssp.flength,ssp.fshift,data.fs),ssp.win))).^2) + 0.1);	% Mel-cepstra
y = bsxfun(@minus,y,sum(y,2)/size(y,2));											% meannorm
dy = filter(ssp.delta,1,y,[],2);
dy = [dy(:,ssp.delta_pos*ssp.delta_rep),dy(:,ssp.delta_pos:end),dy(:,end*ssp.delta_rep)];
ddy = filter(ssp.delta,1,dy,[],2);
ddy = [ddy(:,ssp.delta_pos*ssp.delta_rep),ddy(:,ssp.delta_pos:end),ddy(:,end*ssp.delta_rep)];
y = [y;dy;ddy];
return;

function y=lifter(y,ssp)
y=bsxfun(@times,y,ssp.lift);
return;

function y = show_cep(ssp,y)
y = ssp.idct_inv'*y(1:end/3,:);
if(nargout == 0)
  imagesc([0 size(y,2)*ssp.fshift],[1 size(y,1)],y);axis('xy');colorbar;
  xlabel('Tijd (sec)');
  ylabel('Mel-band');
end
return;
