function y=preproc_do(preproc,x)
% function y=preproc_lifter(preproc,x)
%
% Preprocess the samples x (pre-empahsis -> framing -> windowing -> FFT -> MEL-filterbanks -> log -> Cepstra -> add derivatives).
% See preproc_make() for setting the preprocessing parameters.
% Liftering (only needed for DTW) is not applied (see preproc_lifter()).
%
% Use the following construct to obtain 'smoothed' Mel-spectra to visualize the static features:
%  Y = preproc.idct_inv'*y(1:end/3,:);

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

y = preproc.idct'*log(full(preproc.mel_fb'*abs(fft(bsxfun(@times,frame(x,preproc.flength,preproc.fshift,preproc.fs),preproc.win))).^2) + 0.1);	% Mel-cepstra
y = bsxfun(@minus,y,sum(y,2)/size(y,2));										% meannorm
dy = filter(preproc.delta,1,y,[],2);
dy = [dy(:,preproc.delta_pos*preproc.delta_rep),dy(:,preproc.delta_pos:end),dy(:,end*preproc.delta_rep)];
ddy = filter(preproc.delta,1,dy,[],2);
ddy = [ddy(:,preproc.delta_pos*preproc.delta_rep),ddy(:,preproc.delta_pos:end),ddy(:,end*preproc.delta_rep)];
y = [y;dy;ddy];
return;
