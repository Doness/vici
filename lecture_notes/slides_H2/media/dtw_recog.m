function [word,ndx,s] = dtw_recog(y);
% Usage: [word,ndx,s] = dtw_recog(y)
%
%   x       = input utterance (parameters)
%   word    = recognized word (string)
%   ndx     = index of best scoring template (reference words)
%   s       = scores of all words
%   dtw_cfg : global variable, see dtw_train for more info

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

global dtw_cfg
s = zeros(length(dtw_cfg.voc),1);
for i = 1:length(dtw_cfg.voc)
  s(i) = dtw(dtw_cfg.voc(i).tpl,y);
end

[score,ndx] = min(s);
word = dtw_cfg.wlist{dtw_cfg.voc(ndx).iword};
