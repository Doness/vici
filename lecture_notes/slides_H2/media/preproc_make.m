function preproc=preproc_make(fs,flength,fshift,Ncep,preemp,window)
% function preproc=preproc_make(fs,flength,fshift,Ncep,preemp,window)
%
% Create all data structures needed to quickly perfrom the preprocessing
%   (pipeline: pre-empahsis -> framing -> windowing -> FFT
%             MEL-filterbanks -> log -> Cepstra -> add derivatives)
%
% Default values:
%	fs	16000		Ncep	12
%	flength	0.032		preemp	0.95
%	fshift	0.01		window	@hamming

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

if(nargin<1)	fs=[];		end;	if(isempty(fs))		fs=16000;		end
if(nargin<2)	flength=[];	end;	if(isempty(flength))	flength=0.032;		end
if(nargin<3)	fshift=[];	end;	if(isempty(fshift))	fshift=0.01;		end
if(nargin<4)	Ncep=[];	end;	if(isempty(Ncep))	Ncep=12;		end
if(nargin<5)	preemp=[];	end;	if(isempty(preemp))	preemp=0.95;		end
if(nargin<6)	window=[];	end;	if(isempty(window))	window=@hamming;	end

if(ischar(window))	window=str2func(window);	end;

preproc = struct('flength',flength,'fshift',fshift,'fs',fs,'Ncep',Ncep);
K = [floor(preproc.fs*preproc.flength+0.5),2^ceil(log2(preproc.fs*preproc.flength))];
preproc.win = [window(K(1));zeros(K(2)-K(1),1)];
preproc.mel_fb    = sparse(make_mel_fb(K(2),fs,100));
K = size(preproc.mel_fb,2);
preproc.idct      = cos(pi*((1:K)-0.5)'*(0:K-1)/K);
preproc.idct_inv  = inv(preproc.idct);
preproc.idct      = preproc.idct(:,1:preproc.Ncep+1);
preproc.idct_inv  = preproc.idct_inv(1:preproc.Ncep+1,:);
preproc.delta     = [2,1,0,-1,-2]/10;
preproc.delta_pos = numel(preproc.delta);
preproc.delta_rep = ones(1,(preproc.delta_pos-1)/2);
K = (sin((0:preproc.Ncep)'*(pi/preproc.Ncep))+1)*.1;
preproc.lift      = [K;K*2;K*.5];
return;
