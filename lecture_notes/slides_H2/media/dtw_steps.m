function dtw_steps(action,wsel)

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

% load the data (fixed for now)
[x,fs]=audioread('dtw_som.wav','native');x=double(x(:));
% make preprocessing
ssp = preproc_make(fs);
% preprocess data
s = preproc_do(ssp,x);
x = s(:,280:323); x=(x(:,1:2:end)+x(:,2:2:end))/2;			% test
X = ssp.idct_inv'*x(1:end/3,:); x = preproc_lifter(ssp,x); lx = size(x,2);
y = s(:,132:173); y=(y(:,1:2:end)+y(:,2:2:end))/2;			% reference
Y = ssp.idct_inv'*y(1:end/3,:); y = preproc_lifter(ssp,y); ly = size(y,2);
% make figure
pos = get(0,'ScreenSize');
dtw_fig = figure('Name','DTW-demo','NumberTitle','off','MenuBar','none','Position',[max(ceil((pos(3)-960)/2),1) , max(ceil((pos(4)-8.0)/2),1) , min(pos(3),960) , min(pos(4),720)]);
pos = get(dtw_fig,'Position');
% the windows
ncol=64;colormap([jet(ncol);gray(ncol)]);
s = .4/max(lx,ly);
r = pos(3)/pos(4);
h=axes('Units','normalized','Position',[.00 .00 0.01 0.01]);plot(0,0,'.','Color',[.99 .99 .99]);set(h,'Visible','off');
h=axes('Units','normalized','Position',[.99 .99 0.01 0.01]);plot(0,0,'.','Color',[.99 .99 .99]);set(h,'Visible','off');
% plot x-spectrum
hx=axes('Units','normalized','Position',[0.12 0.12 lx*s  0.1]);
c=X;mn=min(c(:));mx=max(c(:));sv=(ncol-.01)/(mx-mn+1e-99);image(floor((1-mn*sv)+c*sv));axis('xy');
set(hx,'Visible','off');
% plot y-spectrum
hy=axes('Units','normalized','Position',[0.02 0.22  0.1 ly*s*r]);
c=Y;mn=min(c(:));mx=max(c(:));sv=(ncol-.01)/(mx-mn+1e-99);image(flipud(floor((1-mn*sv)+c*sv))');axis('xy');
set(hy,'Visible','off');
% compute + plot local distance matrix
hD=axes('Units','normalized','Position',[0.12 0.22 lx*s ly*s*r]);
D = bsxfun(@minus,reshape(x,[],1,lx),reshape(y,[],ly,1)); D=squeeze(dot(D,D));
c=D.^.6;mn=min(c(:));mx=max(c(:));mxp=max(max(min(c,[],2)),max(min(c,[],1)));sv=ncol/(1e-99+min(.5*(mx-mn),3*(mxp-mn)));image(max(ncol+1,min(2*ncol,floor((ncol+1-mn*sv)+c*sv))));axis('xy');set(hD,'Visible','off');
% plot the cumulative distances + paths 
hd=axes('Units','normalized','Position',[0.55 0.22-s*r (lx+2)*s (ly+2)*s*r]);
% prepare for DTW
d = inf(ly+2,lx+2);
B = nan(ly+1,lx+1);
posx = 1;
posy = 1;
d(1,1) = 0;
% visualize DTW
dp = ones((ly+2)*9,(lx+2)*9)*(ncol*.1);
for i=1:lx+2; dp(:,i*9)=(ncol*.1)+4;	end;
for i=1:ly+2; dp(i*9,:)=(ncol*.1)+4;	end;
dp(1:8,1:8) = ncol+1;
id=image([-4:9:size(dp,2)]/9,[-4:9:size(dp,1)]/9,dp);axis('xy');
hold('on');
ia=plot(zeros(2,3),zeros(2,3),'r','LineWidth',1);
colb=[1,.5,.5];
hb=plot([0,0],[0,0],'.','MarkerSize',5,'Color',colb);
% buttons
uicontrol('Style','pushbutton','Parent',dtw_fig,'Units','normalized','Position',[0.55 0.02 0.08 0.06],'String','>','Callback',@do_step1);
uicontrol('Style','pushbutton','Parent',dtw_fig,'Units','normalized','Position',[0.65 0.02 0.08 0.06],'String','>>','Callback',@do_step2);
uicontrol('Style','pushbutton','Parent',dtw_fig,'Units','normalized','Position',[0.75 0.02 0.08 0.06],'String','>>>','Callback',@do_step3);
uicontrol('Style','pushbutton','Parent',dtw_fig,'Units','normalized','Position',[0.91 0.02 0.08 0.06],'String','Quit','Callback',@do_quit);
h1=uicontrol('Style','text','Parent',dtw_fig,'Units','normalized','Position',[0.55 0.85 0.1 0.03],'String','0.0');
h2=uicontrol('Style','text','Parent',dtw_fig,'Units','normalized','Position',[0.55 0.90 0.1 0.03],'String','0.0');
h3=uicontrol('Style','text','Parent',dtw_fig,'Units','normalized','Position',[0.55 0.95 0.1 0.03],'String','0.0');
h4=uicontrol('Style','text','Parent',dtw_fig,'Units','normalized','Position',[0.75 0.95 0.1 0.03],'String','0.0');
h5=uicontrol('Style','text','Parent',dtw_fig,'Units','normalized','Position',[0.70 0.85 0.1 0.03],'String','0.0');
hk=uicontrol('Style','checkbox','Parent',dtw_fig,'Units','normalized','Position',[0.81 0.85 0.12 0.03],'String','x[1.3,1.0,1.3]','Callback',@do_cost);
K = [1;1;1];
arrow = zeros(3,1);
arrow(3) = annotation('arrow',[.65 .745],[.860 .960],'HeadWidth',6,'HeadLength',8);
arrow(2) = annotation('arrow',[.65 .745],[.915 .965],'HeadWidth',6,'HeadLength',8);
arrow(1) = annotation('arrow',[.65 .745],[.970 .970],'HeadWidth',6,'HeadLength',8);
% that's all
 
function do_cost(source,event,handles,varargin)
K=[1;1;1]+[.3;0;.3]*get(hk,'Value');
end;

function do_step1(source,event,handles,varargin)
% computer upper and lower bounds
L = max(ceil(posx/2), ly-2*(lx-posx)-1);
H = min(posx*2, ly-floor((lx-posx)/2));
posy = max(posy,L);
% update (1 step)
if(posx>lx)
  Dx = 0.0;
else
  Dx = D(posy,posx);
end
src = [d(posy+1,posx);d(posy,posx);d(max(posy-1,1),posx)];
[d(posy+1,posx+1),o] = min(src+Dx*K);
B(posy,posx) = o-1;
dp(9*posy+1:9*posy+8,9*posx+1:9*posx+8) = ncol+min(max((d(posy+1,posx+1)-min(d(:,posx)))*1.5,1),ncol);
set(id,'CData',dp);
set(h1,'String',sprintf('%.2f',src(1)));
set(h2,'String',sprintf('%.2f',src(2)));
set(h3,'String',sprintf('%.2f',src(3)));
set(h4,'String',sprintf('%.2f',d(posy+1,posx+1)));
set(h5,'String',sprintf('%.2f',Dx));
set(arrow(1),'LineWidth',1);
set(arrow(2),'LineWidth',1);
set(arrow(3),'LineWidth',1);
set(arrow(o),'LineWidth',3);
set(ia(1),'Xdata',[-1;0]+posx);set(ia(1),'Ydata',max([-2;0]+posy,0));
set(ia(2),'Xdata',[-1;0]+posx);set(ia(2),'Ydata',[-1;0]+posy);
set(ia(3),'Xdata',[-1;0]+posx);set(ia(3),'Ydata',[-0;0]+posy);
set(hb,'Color',colb,'LineWidth',1);
hb=plot([posx-1,posx],[posy-(o-1),posy],'r','LineWidth',3);
if(posx>lx)					% full back-tracking
  b = zeros(1,lx+2);
  b(end) = ly+1;
  for i=lx:-1:0
    b(i+1) = b(i+2)-B(b(i+2),i+1);
  end
  plot([0:lx+1],b,'r-','LineWidth',3);
end
% taken the next step
if(posy==H)
  posy = 1;
  posx = min(posx+1,lx+1);
else
  posy = posy+1;
end
end

function do_step2(source,event,handles,varargin)
pposx = min(posx,lx);
do_step1();
pause(0.2);
while(posx == pposx)
  do_step1();
  pause(0.2);
end
end

function do_step3(source,event,handles,varargin)
pposx = min(posx,lx);
do_step1();
while(posx == pposx)
  do_step1();
end
drawnow;
end

function do_quit(source,event,handles,varargin)
close(dtw_fig);
end

end
