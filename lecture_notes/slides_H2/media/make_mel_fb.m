function FB = make_mel_fb(nfft,fs,W)
FB = zeros(nfft,floor(f2m(fs/2)/W)-1);
F = nfft/fs;
fn = (0:nfft-1)';
for cm=1:size(FB,2)
  l = m2f((cm-1)*W)*F;
  c = m2f(cm*W)*F;
  h = m2f((cm+1)*W)*F;
  t = min(max((fn-l)/(c-l),0),1).*min(max((h-fn)/(h-c),0),1);
  c = min(max(round(c),1),nfft/2+1);
  t(c) = max(t(c),.1);
  FB(:,cm) = t/sum(t);
end
return;

function m=f2m(f)
m = 1125*log(1+f/700);
return;

function f=m2f(m)
f = 700*(exp(m/1125)-1);
return;

