function ndx = dtw_train(y,word_str)
% Usage: ndx = dtw_train(y,word_str)
%
%   y       = reference utterance (parameters)
%   word    = word string
%   dtw_cfg : global variable, recognition info
%             dtw_cfg.wlist          = { array of word strings }
%             dtw_cfg.voc(ndx).tpl   = reference pattern <ndx>
%             dtw_cfg.voc(ndx).iword = index of the corresponding word string
%
% Returns the index <ndx> of the reference pattern (template).

% Copyright (C) 2014 UGent
% Author: Kris Demuynck

global dtw_cfg
if(~isfield(dtw_cfg,'voc'))
  dtw_cfg.wlist = [];
  dtw_cfg.voc   = [];
end
ndx = length(dtw_cfg.voc)+1;

dtw_cfg.voc(ndx).tpl   = y;
dtw_cfg.voc(ndx).iword = [];
for i=1:length(dtw_cfg.wlist)
  if(strcmp(word_str,dtw_cfg.wlist{i}))
    dtw_cfg.voc(ndx).iword = i;
  end
end
if(isempty(dtw_cfg.voc(ndx).iword))
  i = length(dtw_cfg.wlist)+1;
  dtw_cfg.wlist{i} = word_str;
  dtw_cfg.voc(ndx).iword = i;
end
