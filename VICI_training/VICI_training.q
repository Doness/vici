import "system.q"
import "inttypes.q"
import "string.q"
import "speech_deeplearning.q"

function [] = main()
    %read in the .wav file
    [samples,sample_rate] = load_WAV("VICI_training.wav")
%    plot(samples[0..sample_rate - 1])
    [mel_cepstra, mel_batch_rate] = extract_mel_cepstra(samples,sample_rate)
    
    %read in the groundtruth, once for the raw input and once for the mel cepstra input
%    [groundtruth_samples,mask_samples] = load_groundtruth("VICI_training.annotation",numel(samples),sample_rate)
    [cepstra_gt,cepstra_mask] = load_groundtruth("VICI_training.annotation",size(mel_cepstra,0),mel_batch_rate)
    
    train_network_cepstra(mel_cepstra,mel_batch_rate,cepstra_gt,cepstra_mask)
end

function [] = train_network_cepstra(cepstra:mat,sample_rate:scalar,gt:mat,mask:mat)    
    network = initDeepLearningNetwork()
    network.set_loss_function("binary")
    network.add_layer(get_biased_convolve_layer(radius=int(sample_rate/2),channels_in=13,channels_out=32))
    network.add_layer(get_leaky_RELU_layer(leakage:scalar = 0.1))
    network.add_layer(get_biased_convolve_layer(radius=int(sample_rate/2),channels_in=32,channels_out=8))
    network.add_layer(get_leaky_RELU_layer(leakage:scalar = 0.1))
    network.add_layer(get_biased_convolve_layer(radius=int(sample_rate/2),channels_in=8,channels_out=1))
    
    network.settings.rate = 1e-4
    for i = 1..2
        network.train(cepstra,gt,1000,mask,visualisation=true)
        network.settings.rate = network.settings.rate*0.8
    end
    
    network.save_to_file("E:\\Programming\\vici\\VICI_training\\quasar_machines","VICI_spotting_cepstra")
end

function [groundtruth:vec,mask:vec] = load_groundtruth(name:string,nr_samples:int,sample_rate:scalar)
    fh = fopen(name,"rt")
    nr_positives = fscanf(fh,"%d")[0]
    groundtruth_times = zeros(nr_positives)
    for i = 0..nr_positives-1
        groundtruth_times[i] = fscanf(fh,"%f")[0]
    end
    
    groundtruth = zeros(nr_samples,1)
    mask = ones(nr_samples,1)
    mask_width = int(0.5*sample_rate)
    
    groundtruth_indices = int(groundtruth_times * sample_rate)
    for i =0..nr_positives-1
        idx = groundtruth_indices[i]
        groundtruth[idx] = 1
        mask[idx-mask_width..idx+mask_width] = 0
        mask[idx] = 1
    end
end

function [melcepstra:mat,batch_rate:scalar] = extract_mel_cepstra(samples:vec,sample_rate:scalar)
    batch_rate = 40%per second (should be between 25 and 40)
    output_cepstra = 13
    overlap = 1/3
    N = round(sample_rate/batch_rate/overlap) %the window length
    frequency_ratio = batch_rate*overlap
    
    nr_windows = floor(numel(samples)/N/overlap)
    windows = zeros(nr_windows,N)
    function [] = __kernel__ window_kernel(pos:ivec1)
        windows[pos,0] = 0.0
        maxx = 0.0
        for n = 1..N-1
            %step 1: pre-emphasis
            %high-pass filter to model the cochlea
            value = samples[pos*N*overlap+n] - 0.95*samples[pos*N*overlap+n-1]
            %step 2: windowing
            % use a windowing function (a hamming window)
            windowed_value = value * (0.54-0.46*cos(2*pi*n/(N-1)))
            windows[pos,n] = windowed_value
            maxx ^^= windowed_value
        end
        
        for n = 0..N-1
            windows[pos,n] = windows[pos,n]/maxx
        end
    end
    parallel_do(nr_windows,window_kernel)
    
    %step 3: spectral analysis - get the spectral density
    powerspectra = abs(fft1(windows)).^2
    
    %step 4: the mel-spectrum (project using the standard triangle windows)
    frequency_range = 4000 %we only attempt up to 4 kHz (should be plenty for speech)
    nr_mel_scales = int(hz_to_mel(frequency_range)/100)-1
    melspectra = zeros(nr_windows,nr_mel_scales)
    function [] = __kernel__ mel_kernel(powerspectra:mat,melspectra:mat,nr_mel_scales:int,pos:ivec1)
        for mel = 0..nr_mel_scales-1
            prev_idx = hz_to_idx(mel_to_hz(100*mel),frequency_ratio)
            this_idx = hz_to_idx(mel_to_hz(100*(mel+1)),frequency_ratio)
            next_idx = hz_to_idx(mel_to_hz(100*(mel+2)),frequency_ratio)
            val = 0.0
            cfs = 0.0
            for n = 0..N-1
                if (n>=prev_idx && n < this_idx)
                    val += (n-prev_idx)/(this_idx-prev_idx) * powerspectra[pos,n]
                    cfs += (n-prev_idx)/(this_idx-prev_idx)
                endif
                if (n == this_idx)
                    val += powerspectra[pos,n]
                    cfs += 1.0
                endif
                if (n>this_idx && n < next_idx)
                    val += (n-this_idx)/(next_idx-this_idx) * powerspectra[pos,n]
                    cfs += (n-this_idx)/(next_idx-this_idx)
                endif
            end
            melspectra[pos,mel] = val/cfs
        end
    end
    parallel_do(nr_windows,powerspectra,melspectra,nr_mel_scales,mel_kernel)
    
    %step 5: dynamic compression (log-mel scale)
    logmelspectra = log(melspectra+0.1)
    
    %step 6: the mel cepstrum (dct of the log-mel spectrum)
    %fft zero padded
    melcepstrac = fft1(cat(logmelspectra,zeros(size(logmelspectra))))[0..nr_windows-1,1..output_cepstra]
    %half-sample phase shift of the first half
    function [] = __kernel__ phase_kernel(melcepstrac:cmat,output_cepstra:int,nr_mel_scales:int,pos:ivec1)
        for mel = 0..output_cepstra-1
            melcepstrac[pos,mel] = melcepstrac[pos,mel]*2*exp(-1i*pi*(mel+1)/2/nr_mel_scales)
        end
    end
    parallel_do(nr_windows,melcepstrac,output_cepstra,nr_mel_scales,phase_kernel)
    melcepstra = real(melcepstrac)
end

%ratio = samplerate/batchsize*overlap
function [hz:scalar] = __device__ idx_to_hz(idx:scalar,ratio:scalar)
    hz = idx * ratio
end

%ratio = samplerate/batchsize*overlap
function [idx:int] = __device__ hz_to_idx(hz:scalar,ratio:scalar)
    idx = int(hz/ratio)
end

function [hz:scalar] = __device__ mel_to_hz(mel:scalar)
    hz = 700 * (10^(mel/2595) - 1)
end

function [mel:scalar] = __device__ hz_to_mel(hz:scalar)
    mel = 2595 * log10(1 + hz / 700)
end
    
function [samples,sample_rate] = load_WAV(name:string)
    fh = fopen(name,"rb")
    header = uint8_to_float(fread(fh,44,"uint8"))
    assert(all(("RIFF")[:] == header[0..3]))
    little_endian = [1,2^8,2^16,2^24]
    file_size = sum(header[4..7].*little_endian)
    assert(all(("WAVE")[:] == header[8..11]))
    assert(all(("fmt ")[:] == header[12..15]))
    subchunk1_size = sum(little_endian.*header[16..19])
    audio_format = sum(little_endian[0..1].*header[20..21])
    assert(audio_format == 1)%uncompressed PCM
    num_channels = sum(little_endian[0..1].*header[22..23])
    assert(num_channels == 1)%this code can only handle a single channel
    sample_rate = sum(little_endian.*header[24..27])
    byte_rate = sum(little_endian.*header[28..31])
    block_align = sum(little_endian[0..1].*header[32..33])
    bits_per_sample = sum(little_endian[0..1].*header[34..35])
    %just scan forward in the file until we encounter "data"
    bytes_left = file_size - 36;
    repeat
        bytes = uint8_to_float(fread(fh,4,"uint8"))
        bytes_left = bytes_left - 4
    until all(("data")[:] == bytes[0..3])
    
    samples = uint8_to_float(fread(fh,bytes_left,"uint8"))
    
    %if 16-bit, samples are saved in 2-complement´s little endian
    %big endianness, so fread can´t do it correctly
    match bits_per_sample with
    | 16-> 
        samples = samples[0..2..bytes_left-1]+2^8*samples[1..2..bytes_left-1]
        samples = samples - 2^16*(samples >= 2^15)
    end
end