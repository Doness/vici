%====================================================================
% Quasar library for deep learning training and processing for speech
% March 2016 - Simon Donné
%====================================================================
import "Quasar.UI.dll"
import "imresize.q"

{!author name="Simon Donné"}
{!doc category="Machine Learning"}

type Data : mat
type Parameter : cube
type Parameters : vec[Parameter]
type ForwardCNNFunction  : [(Data,Parameters)->(Data)] %(input, parameters) -> (output)
type BackwardCNNFunction : [(Data,Data,Parameters,Data)->(Data,Parameters)] %(input,output,parameters,dzdoutput)->(dzdinput,dzdparameters)
type DeepLearningLayer : class
    name : string
    forward_pass  : ForwardCNNFunction
    backward_pass : BackwardCNNFunction
    parameters : Parameters
    shrink_rates : vec
endtype
type DeepLearningLayers : vec[^DeepLearningLayer]
type DeepLearningLossFunction : [(Data,Data)->(Data,Data)]
type DeepLearningSettings : mutable class
    rate:scalar
    momentum:scalar
    plot_period:int
endtype
type DeepLearningNetwork : mutable class
    loss : DeepLearningLossFunction
    loss_name : string
    layers : DeepLearningLayers
    settings : ^DeepLearningSettings
    nr_layers : int
endtype

% Function: DeepLearningFrameworkVersion
% Returns the current version of the deep learning framework.
% The version number should be used as follows:
%   - changes behind the comma should be backwards compatible (i.e. a v1.6 network can be read in by the v1.7 framework, but not necessarily vice versa -- although an effort will be made)
%   - changes in the integer part of the version number are not compatible at all (i.e. a v2.0 is not legible by either v1.0 or v3.0)
% As such, when you add layer types, please increment the number behind the comma
%
% : function [version:scalar] = DeepLearningFrameworkVersion()
%
function [version:scalar] = DeepLearningFrameworkVersion()
    version = 0.1
endfunction

% Function: DeepLearningNetwork.load_from_file
% Loads a pre-trained network from the hard disk, with checks for the framework version
% Must be called on a pre-initialized DeepLearningNetwork
%
% : function [] = load_from_file(self:^DeepLearningNetwork,filepath:string,network_name:string)
%
% See also:
%    <initDeepLearningNetwork>,<DeepLearningFrameworkVersion>,<save_to_file>
%
function [] = load_from_file(self:^DeepLearningNetwork,filepath:string,network_name:string)
    %test for legible error in case the path does not exist yet
    dir(strcat(filepath,"/*"))
    
    %load the meta file: framework version, loss function info, layer info, training settings
    metafilename = sprintf("%s/%s.meta",filepath,network_name)
    fh = fopen(metafilename,"rt")
    
    %sanity check on the text file
    if fgets(fh)!="Quasar deep learning network"
        error("This is not a quasar deep learning network!")
    endif
    
    %version sanity check
    that_version = sscanf(fgets(fh),"%f")[0]
    this_version = DeepLearningFrameworkVersion()
    if int(that_version) > int(this_version)
        error("The network was created with a newer version of the framework that is not supported.")
    elseif that_version > this_version + 1e-3
        fprintf("WARNING> The network was created with a newer version of the framework. Trying to load it, but some features might be unsupported.")
    elseif int(that_version) < int(this_version)
        error("The network was created with a version of the framework that is no longer supported.")
    endif
    
    %get the network parameters
    loss_name = fgets(fh)
    self.set_loss_function(loss_name)
    self.nr_layers = sscanf(fgets(fh),"%d")[0]
    self.layers = DeepLearningLayers(self.nr_layers*2)
    for layer = 0..self.nr_layers-1
        layer_name = fgets(fh)
        match layer_name with
        | "convolution"         ->  self.layers[layer] = get_convolve_layer()
        | "biased_convolution"  ->  self.layers[layer] = get_biased_convolve_layer()
        | "sigmoid"             ->  self.layers[layer] = get_sigmoid_layer()
        | "tanh"                ->  self.layers[layer] = get_tanh_layer()
        | "ReLU"                ->  self.layers[layer] = get_RELU_layer()
        | "leaky ReLU"          ->  self.layers[layer] = get_leaky_RELU_layer()
        | _ -> error(sprintf("Unknown layer type: %s",layer_name))
        endmatch
    endfor
    self.settings.rate = sscanf(fgets(fh),"%f")[0]
    self.settings.momentum = sscanf(fgets(fh),"%f")[0]
    self.settings.plot_period = sscanf(fgets(fh),"%d")[0]
    
    %read in each of the layers' parameters
    for layer = 0..self.nr_layers-1
        layerfilename = sprintf("%s/%s.layer%d",filepath,network_name,layer)
        [tparam] = load(layerfilename)
        self.layers[layer].parameters[:]=tparam
    endfor
endfunction

% Function: DeepLearningNetwork.eexists_on_file
% Checks whether a given network exists in a certain location. Does not perform sanity checks on the network itself (yet).
%
% : function [] = exists_on_file(self:^DeepLearningNetwork,filepath:string,network_name:string)
%
% See also:
%    <initDeepLearningNetwork>,<load_from_file>,<save_to_file>
%
function [exists:int]= exists_on_file(self:^DeepLearningNetwork,filepath:string,network_name:string)
    exists = exist(sprintf("%s/%s.meta",filepath,network_name), "file")
endfunction

% Function: DeepLearningNetwork.save_to_file
% Save a network to the hard disk. Must be called on a pre-initialized DeepLearningNetwork.
%
% : function [] = save_to_file(self:^DeepLearningNetwork,filepath:string,network_name:string)
%
% See also:
%    <initDeepLearningNetwork>,<DeepLearningFrameworkVersion>,<load_from_file>
%
function [] = save_to_file(self:^DeepLearningNetwork,filepath:string,network_name:string)
    %test for legible error in case the path does not exist yet
    dir(strcat(filepath,"/*"))
    
    %save the meta file: framework version, loss function info, layer info, training settings
    metafilename = sprintf("%s/%s.meta",filepath,network_name)
    fh = fopen(metafilename,"wt")
    fprintf(fh,"Quasar deep learning network\n")
    fprintf(fh,"%1.2f\n",DeepLearningFrameworkVersion())
    fprintf(fh,"%s\n",self.loss_name)
    fprintf(fh,"%d\n",self.nr_layers)
    for layer = 0..self.nr_layers-1
        fprintf(fh,"%s\n",self.layers[layer].name)
    endfor
    fprintf(fh,"%f\n",self.settings.rate)
    fprintf(fh,"%f\n",self.settings.momentum)
    fprintf(fh,"%d\n",self.settings.plot_period)
    fclose(fh)
    
    %for each layer, save a separate (binary) file with its parameter values
    for layer = 0..self.nr_layers-1
        layerfilename = sprintf("%s/%s.layer%d",filepath,network_name,layer)
        save(layerfilename,copy(self.layers[layer].parameters))
    endfor
endfunction

% Function: DeepLearningNetwork.train_multiple
% Trains a pre-constructed network given a set of inputs and groundtruths. At each iteration, the subsequent input-groundtruth pair will be taken, while preserving the momentum.
% The optional arguments (in order):
%   - the number of gradient descent iterations (integer, defaults to 100)
%   - the masks: only groundtruth pixels for which masks[img][pos] is set to 1 are counted in the error criterion (same dimensions as groundtruth, defaults to all 1's)
%   - the visualization: boolean value to indicate whether the GUI is shown during optimization. The refresh rate is controlled using DeepLearningNetwork.settings.plot_period
%
% The DeepLearningNetwork contains a settings field, comprising
%   - network.settings.rate: step size for the gradient descent (defaults to 1e-3, which is plausible for groundtruth in [0,1])
%   - network.settings.momentum: momentum for the stochastic gradient descent (in [0,1], higher will mean more regularized steps - defaults to 0.85)
%   - network.settings.plot_period: refresh rate for the UI when visualization is turned on
%
% The training method is so-called Stochastic Gradient Descent, which uses a momentum term, a shrinkage term, and standard gradient descent.
%   The momentum is a network-wide setting, whereas the shrinkage terms are set per-parameter (each layer for its own, in layer.shrink_rates).
%
% : function [] = train_multiple(self:^DeepLearningNetwork,input:Data,groundtruth:Data,nr_iterations:int=100,mask:Data=zeros(0,0,0,0),visualization:int=true)
%
% See also:
%    <initDeepLearningNetwork>,<train>
%
function [] = train_multiple(self:^DeepLearningNetwork,inputs:vec[Data],groundtruths:vec[Data],nr_iterations:int=100,masks:vec[Data]=vec[Data](0),visualization:int=true)
    %learning with stochastic gradient descent: parameters
    rate = self.settings.rate
    momentum = self.settings.momentum
    plot_period = self.settings.plot_period
    if plot_period == 0
        visualization = false
    endif
    
    nr_inputs = numel(inputs)
    assert(numel(groundtruths) == nr_inputs)
    
    summasks = zeros(nr_inputs)
    pos_counts = zeros(nr_inputs)
    neg_counts = zeros(nr_inputs)
    if numel(masks) == 0
        masks = vec[Data](nr_inputs)
        for img = 0..nr_inputs-1
            masks[img] = ones(size(groundtruths[img]))
        endfor
    endif
    for img = 0..nr_inputs-1
        if numel(masks[img]) < numel(groundtruths[img])
            masks[img] = ones(size(groundtruths[img]))
        endif
        summasks[img] = sum(masks[img])
        pos_counts[img] = max(sum(groundtruths[img] >= 1),1e-9)
        neg_counts[img] = max(sum(groundtruths[img] <= 0),1e-9)
    endfor
    
    
    %preparing the visualization, if necessary
    if visualization
        frm = form("Deep learning network training for speech")
        hgr_plt = frm.add_horizontallayout()
        dsp_plt = hgr_plt.add_display()
        dsp_hst = hgr_plt.add_display()
    endif
    
    %optimization variables
    parameter_momentum = vec[Parameters](self.nr_layers)
    for i = 0..self.nr_layers-1
        nr_params = numel(self.layers[i].parameters)
        parameter_momentum[i] = Parameters(nr_params)
        for j = 0..nr_params-1
            parameter_momentum[i][j] = zeros(size(self.layers[i].parameters[j]))
        endfor
    endfor
    
    errors = zeros(nr_inputs)
    error_log = zeros(nr_iterations)
    for iteration = 0..nr_iterations-1
%        img_idx = mod(floor((iteration+1)/plot_period),nr_inputs)
        img_idx = mod(iteration,nr_inputs)
        input = inputs[img_idx]
        groundtruth = groundtruths[img_idx]
        mask = masks[img_idx]
        summask = summasks[img_idx]
        pos_count = pos_counts[img_idx]
        neg_count = neg_counts[img_idx]
        
        tic()
        %the forward pass
        layer_output = self.process_full(input)
        output = layer_output[self.nr_layers-1]

        %the current derivative with respect to the output
        [z,dzdoutput] = self.loss(output,groundtruth)
        %apply an ignore mask around the borders of the groundtruth
        z = z .* mask
        dzdoutput = dzdoutput .* mask
        %save the error for fitting
        errors[img_idx] = sum(z)/summask
        error_log[iteration] = sum(errors)/max(sum(errors>0),1e-9)
        
        %for each layer, backward propagation, updating the momentum and taking the step
        for i = self.nr_layers-1..-1..0
            [dzdoutput,dzdparameters] = self.layers[i].backward_pass(i==0?input:layer_output[i-1],layer_output[i],self.layers[i].parameters,dzdoutput)
            %normalize the input step energy: otherwise the step size lowers as the error lowers, but it does so too fast!
            nr_params = numel(self.layers[i].parameters)
            for j = 0..nr_params-1
                grad:Parameter = dzdparameters[j]
                grad = grad / sqrt(sum(grad.^2)+eps)
                shrink:Parameter = self.layers[i].shrink_rates[j]*self.layers[i].parameters[j]
                parameter_momentum[i][j] = parameter_momentum[i][j]*momentum + rate*(grad+shrink)
                self.layers[i].parameters[j] = self.layers[i].parameters[j] - parameter_momentum[i][j]
            endfor
        endfor
        
        if visualization && (mod(iteration+1,plot_period) == 0 || iteration == 0)
            printf("CNN SGD iteration %d: %f ms",iteration+1,toc(""))            
            %the plot output: error functions
            dsp_plt.plot(1..iteration+1,error_log[0..iteration],"b")%data fit
            xlim([1,nr_iterations])
            ylim([0,max(error_log[iteration]*1.5,1e-9)])
            title("Data fit error")
            hold("off")
            
            %the current score distribution of positive and negative pixels
            %we formulate a histogram between -1 and 2 with 32 bins - only applicable if the groundtruth is in that same range
            
            %currently restricted to the first trained label
            if self.loss_name == "binary"
                hist_length = 128
                hist_steps = -1..(3/(hist_length-1))..2
                pos_hist = zeros(hist_length)
                neg_hist = zeros(hist_length)
                function [] = __kernel__ fill_hist(output:Data,hist_length:int,groundtruth:Data,pos_hist:vec,neg_hist:vec,pos:ivec1)
                    if mask[pos,0]
                        val = output[pos,0]
                        idx:int = int(round((val+1)/3*(hist_length-1)))
                        idx = clamp(idx,hist_length-1)
                        if groundtruth[pos,0] >= 1
                            pos_hist[idx] += 1.0
                        elseif groundtruth[pos,0] <= 0
                            neg_hist[idx] += 1.0
                        endif
                    endif
                endfunction
                parallel_do(size(output),output,hist_length,groundtruth,pos_hist,neg_hist,fill_hist)
                dsp_hst.plot([0.0,0.0],[0.0,1.0],"b")
                hold("on")
                plot([1.0,1.0],[0.0,1.0],"b")
                plot(hist_steps,neg_hist/neg_count,"r")
                plot(hist_steps,pos_hist/pos_count,"g")
                xlim([-1.0,2.0])
                title("Output distributions (red background, green foreground)")
                hold("off")
            endif
        else
            toc("")
        endif
    endfor
    
    if visualization
        frm.close()
    endif
endfunction

% Function: DeepLearningNetwork.train
% Trains a pre-constructed network given the input and groundtruth.
% The optional arguments (in order):
%   - the number of gradient descent iterations (integer, defaults to 100)
%   - the mask: only groundtruth pixels for which mask[pos] is set to 1 are counted in the error criterion (same dimensions as groundtruth, defaults to all 1's)
%   - the visualization: boolean value to indicate whether the GUI is shown during optimization. The refresh rate is controlled using DeepLearningNetwork.settings.plot_period
%
% The DeepLearningNetwork contains a settings field, comprising
%   - network.settings.rate: step size for the gradient descent (defaults to 1e-3, which is plausible for groundtruth in [0,1])
%   - network.settings.momentum: momentum for the stochastic gradient descent (in [0,1], higher will mean more regularized steps - defaults to 0.85)
%   - network.settings.plot_period: refresh rate for the UI when visualization is turned on
%
% The training method is so-called Stochastic Gradient Descent, which uses a momentum term, a shrinkage term, and standard gradient descent.
%   The momentum is a network-wide setting, whereas the shrinkage terms are set per-parameter (each layer for its own, in layer.shrink_rates).
%
% : function [] = train(self:^DeepLearningNetwork,input:Data,groundtruth:Data,nr_iterations:int=100,mask:Data=zeros(0,0,0),visualization:int=true)
%
% See also:
%    <initDeepLearningNetwork>
%
function [] = train(self:^DeepLearningNetwork,input:Data,groundtruth:Data,nr_iterations:int=100,mask:Data=zeros(0,0,0),visualization:int=true)
    inputs=vec[Data](1)
    inputs[0] = input
    groundtruths=vec[Data](1)
    groundtruths[0] = groundtruth
    masks=vec[Data](1)
    masks[0] = mask
    self.train_multiple(inputs,groundtruths,nr_iterations,masks,visualization)
endfunction

% Function: deeplearning_binary_loss
% Internal function. Binary loss function. Please refer to <set_loss_function> instead.
%
% See also:
%    <set_loss_function>
%
function [z:Data,dzdoutput:Data] = deeplearning_binary_loss(output:Data,groundtruth:Data)
    pos_count = max(sum(groundtruth >= 1),1e-9)
    neg_count = max(sum(groundtruth <= 0),1e-9)
    z = max(output.*(1-groundtruth),0)/neg_count + max((1 - output).*groundtruth,0)/pos_count
    z = z*numel(groundtruth)
    dzdoutput = (output > 0).*(1-groundtruth)/neg_count - (output < 1).*(groundtruth)/pos_count
    dzdoutput = dzdoutput*numel(groundtruth)
endfunction

% Function: deeplearning_MSE_loss
% Internal function. MSE loss function. Please refer to <set_loss_function> instead.
%
% See also:
%    <set_loss_function>
%
function [z:Data,dzdoutput:Data] = deeplearning_MSE_loss(output:Data,groundtruth:Data)
    z = (output-groundtruth).^2
    dzdoutput = 2*(output-groundtruth)
endfunction

% Function: deeplearning_MAE_loss
% Internal function. MAE loss function. Please refer to <set_loss_function> instead.
%
% See also:
%    <set_loss_function>
%
function [z:Data,dzdoutput:Data] = deeplearning_MAE_loss(output:Data,groundtruth:Data)
    z = abs(output-groundtruth)
    dzdoutput = sign(output-groundtruth)
endfunction

% Function: deeplearning_empty_loss
% Internal function. Default (unset) loss function. Please refer to <set_loss_function> instead.
% Will throw an error when called, to indicate that a loss function should be set.
%
% See also:
%    <set_loss_function>
%
function [z:Data,dzdoutput:Data] = deeplearning_empty_loss(output:Data,groundtruth:Data)
    z = zeros(size(groundtruth))
    dzdoutput = zeros(size(output))
    error("Please pick a loss function for your network using DeepLearningNetwork.set_loss_function()!")
endfunction

% Function: DeepLearningNetwork.process_full
% Internal function. Full propagation function, which also returns each intermediate result. Please refer to <process> instead.
%
% See also:
%    <process>
%
function [output:vec[Data]] = process_full(self:^DeepLearningNetwork,input:Data)
    output = vec[Data](self.nr_layers)
    output[0] = self.layers[0].forward_pass(input)
    for i = 1..self.nr_layers-1
        output[i] = self.layers[i].forward_pass(output[i-1])
    endfor
endfunction

% Function: DeepLearningNetwork.process
% Network propagation function, which returns the output of the entire network.
%
%   function output:Data = process(self:^DeepLearningNetwork,input:Data)
%
% See also:
%    <train>
%
function [output:Data] = process(self:^DeepLearningNetwork,input:Data)
    output = self.layers[0].forward_pass(input)
    for i = 1..self.nr_layers-1
        output = self.layers[i].forward_pass(output)
    endfor
endfunction

% Function: DeepLearningLayer.forward_pass
% Internal function for the processing performed by a single layer. Please refer to <process> instead.
%
% See also:
%    <process>
%
function [output:Data] = forward_pass(self:^DeepLearningLayer,input:Data)
    output = self.forward_pass(input,copy(self.parameters))
endfunction

% Function: deeplearning_convolve_forward
% Internal function for the processing performed by a convolution layer. Please refer to <get_convolve_layer> instead.
%
% See also:
%    <get_convolve_layer>
%
function [output:Data] = deeplearning_convolve_forward(input:Data,parameters:Parameters)
    w = parameters[0]
    channels_in  = size(w,1)
    channels_out = size(w,2)
    radius = (size(w,0)-1)/2
    
    function [] = __kernel__ imfilter_kernel_nonsep(result,pos : ivec2)
        sum = 0.0
        for dx=-radius..radius
        for c_in = 0..channels_in-1
            sum += w[dx+radius,c_in,pos[1]] * input[pos[0]+dx,c_in]
        endfor
        endfor
    
        result[pos] = sum
    endfunction
    output = uninit(size(input,0),channels_out)
    parallel_do(size(output,0..1),output,imfilter_kernel_nonsep)
endfunction

% Function: deeplearning_convolve_backward
% Internal function for the processing performed by a convolution layer. Please refer to <get_convolve_layer> instead.
%
% See also:
%    <get_convolve_layer>
%
function [dzdinput:Data,dzdparameters:Parameters] = deeplearning_convolve_backward(input:Data,output:Data,parameters:Parameters,dzdoutput:Data)
    w = parameters[0]
    channels_in  = size(w,1)
    channels_out = size(w,2)
    radius = (size(w,0)-1)/2
    
    dzdinput = uninit(size(input))
    dzdw = zeros(size(w))
    function [] = __kernel__ backward_convolution_1(dzdoutput:Data,dzdinput:Data,w:Parameter,radius:int,pos:ivec2)
        %find all pixels that contributed to this location and propagate the derivative
        summ = 0.0
        for dx = -radius..radius
        for c_out  = 0..channels_out-1
            summ += w[dx+radius,pos[1],c_out]*dzdoutput[pos[0]+dx,c_out]
            dzdw[dx+radius,pos[1],c_out] += dzdoutput[pos[0],c_out]*input[pos[0]+dx,pos[1]]
        endfor
        endfor
        dzdinput[pos] = summ
    endfunction
    parallel_do(size(input),dzdoutput,dzdinput,w,radius,backward_convolution_1)
    
    dzdparameters = Parameters(1)
    dzdparameters[0] = dzdw
endfunction

% Function: get_convolve_layer
% Function for acquiring a pre-constructed convolution layer, optionally with a specified radius (defaults to 1, so a 3x3 dilation), and number of input/output channels (defaults to 3/1)
% To be used in conjunction with <add_layer>.
%
%   function [convolve_layer:^DeepLearningLayer] = get_convolve_layer(radius:int=2,channels_in:int=3,channels_out:int=1)
%
% See also:
%    <add_layer>
%
function [convolve_layer:^DeepLearningLayer] = get_convolve_layer(radius:int=2,channels_in:int=3,channels_out:int=1)
    init_param = Parameters(1)
    w:Parameter = randn(2*radius+1,channels_in,channels_out)/10
    w = w - mean(w)
    init_param[0] = w
    rates:vec = [1e-4]
    
    convolve_layer = DeepLearningLayer(name:="convolution",forward_pass:=deeplearning_convolve_forward,backward_pass:=deeplearning_convolve_backward,parameters:=init_param,shrink_rates:=rates)
endfunction

% Function: deeplearning_biased_convolve_forward
% Internal function for the processing performed by a biased convolution layer. Please refer to <get_biased_convolve_layer> instead.
%
% See also:
%    <get_biased_convolve_layer>
%
function [output:Data] = deeplearning_biased_convolve_forward(input:Data,parameters:Parameters)
    filters = parameters[0]
    biases  = parameters[1]
    channels_in  = size(filters,1)
    channels_out = size(filters,2)
    
    center = int((size(filters,0)-1)/2)
    function [] = __kernel__ imfilter_kernel_nonsep(result,pos : ivec2)
        sum = 0.0
        for m=0..size(filters,0)-1
        for c_in = 0..channels_in-1
            sum += filters[m,c_in,pos[1]] * input[clamp(pos[0] + m-center,size(input,0)-1),c_in]
        endfor
        endfor
    
        result[pos] = sum + biases[pos[1],0,0]
    endfunction
    output = uninit(size(input,0),channels_out)
    parallel_do(size(output,0..1),output,imfilter_kernel_nonsep)
endfunction

% Function: deeplearning_biased_convolve_backward
% Internal function for the processing performed by a biased convolution layer. Please refer to <get_biased_convolve_layer> instead.
%
% See also:
%    <get_biased_convolve_layer>
%
function [dzdinput:Data,dzdparameters:Parameters] = deeplearning_biased_convolve_backward(input:Data,output:Data,parameters:Parameters,dzdoutput:Data)
    ws = parameters[0]
    bs = parameters[1]
    channels_in  = size(ws,1)
    channels_out = size(ws,2)
    radius = (size(ws,0)-1)/2
    
    dzdinput = zeros(size(input))
    dzdw  = zeros(size(ws))
    function [] = __kernel__ backward_convolution_1(dzdoutput:Data,dzdinput:Data,w:Parameter,dzdw:Parameter,radius:int,pos:ivec2)
        %find all pixels that contributed to this location and propagate the derivative
        summ = 0.0
        for dx = -radius..radius
        for c_out  = 0..channels_out-1
            summ += w[dx+radius,pos[1],c_out]*dzdoutput[pos[0]+dx,c_out]
            dzdw[dx+radius,pos[1],c_out] += dzdoutput[pos[0],c_out]*input[pos[0]+dx,pos[1]]
        endfor
        endfor
        dzdinput[pos] = summ
    endfunction
    parallel_do(size(input),dzdoutput,dzdinput,ws,dzdw,radius,backward_convolution_1)
    
    dzdb = squeeze(sum(dzdoutput,0))
    
    dzdparameters = Parameters(2)
    dzdparameters[0] = dzdw
    dzdparameters[1] = dzdb
endfunction

% Function: get_biased_convolve_layer
% Function for acquiring a pre-constructed biased convolution layer, optionally with a specified radius (defaults to 1, so a 3x3 kernel), and number of input/output channels (defaults to 3/1)
% To be used in conjunction with <add_layer>.
%
%   function [biased_convolve_layer:^DeepLearningLayer] = get_biased_convolve_layer(radius:int=2,channels_in:int=3,channels_out:int=1)
%
% See also:
%    <add_layer>
%
function [biased_convolve_layer:^DeepLearningLayer] = get_biased_convolve_layer(radius:int=2,channels_in:int=3,channels_out:int=1)
    init_param = Parameters(2)
    w:Parameter = randn(2*radius+1,channels_in,channels_out)/100
    init_param[0] = w - mean(w)
    init_param[1] = zeros(channels_out,1,1,1)
    rates:vec = [1e-4,0]
    
    biased_convolve_layer = DeepLearningLayer(name:="biased_convolution",forward_pass:=deeplearning_biased_convolve_forward,backward_pass:=deeplearning_biased_convolve_backward,parameters:=init_param,shrink_rates:=rates)
endfunction

% Function: deeplearning_sigmoid_forward
% Internal function for the processing performed by a sigmoid activation layer. Please refer to <get_sigmoid_layer> instead.
%
% See also:
%    <get_upsample_NN_layer>
%
function [output:Data] = deeplearning_sigmoid_forward(input:Data,parameters:Parameters)
    output = 1./(1+exp(-input))
endfunction

% Function: deeplearning_sigmoid_backward
% Internal function for the processing performed by a sigmoid activation layer. Please refer to <get_sigmoid_layer> instead.
%
% See also:
%    <get_upsample_NN_layer>
%
function [dzdinput:Data,dzdparameters:Parameters] = deeplearning_sigmoid_backward(input:Data,output:Data,parameters:Parameters,dzdoutput:Data)
    dzdinput = dzdoutput.*output.*(1-output)
    dzdparameters = Parameters(1)
    dzdparameters[0] = [0.0]
endfunction

% Function: get_sigmoid_layer
% Function for acquiring a pre-constructed sigmoid activation layer.
% To be used in conjunction with <add_layer>.
%
%   function [sigmoid_layer:^DeepLearningLayer] = get_sigmoid_layer()
%
% See also:
%    <add_layer>
%
function [sigmoid_layer:^DeepLearningLayer] = get_sigmoid_layer()
    init_param = Parameters(1)
    init_param[0] = [0.0]
    rates:vec = [0.0]
    
    sigmoid_layer = DeepLearningLayer(name:="sigmoid",forward_pass:=deeplearning_sigmoid_forward,backward_pass:=deeplearning_sigmoid_backward,parameters:=init_param,shrink_rates:=rates)
endfunction

% Function: deeplearning_tanh_forward
% Internal function for the processing performed by a hyperbolic tangent activation layer. Please refer to <get_tanh_layer> instead.
%
% See also:
%    <get_tanh_layer>
%
function [output:Data] = deeplearning_tanh_forward(input:Data,parameters:Parameters)
    output = (exp(input)-exp(-input))./(exp(input)+exp(-input))
endfunction

% Function: deeplearning_tanh_backward
% Internal function for the processing performed by a hyperbolic tangent activation layer. Please refer to <get_tanh_layer> instead.
%
% See also:
%    <get_tanh_layer>
%
function [dzdinput:Data,dzdparameters:Parameters] = deeplearning_tanh_backward(input:Data,output:Data,parameters:Parameters,dzdoutput:Data)
    dzdinput = dzdoutput.*(1-output.^2)
    dzdparameters = Parameters(1)
    dzdparameters[0] = [0.0]
endfunction

% Function: get_tanh_layer
% Function for acquiring a pre-constructed hyperbolic tangent activation layer.
% To be used in conjunction with <add_layer>.
%
%   function [sigmoid_layer:^DeepLearningLayer] = get_tanh_layer()
%
% See also:
%    <add_layer>
%
function [tanh_layer:^DeepLearningLayer] = get_tanh_layer()
    init_param = Parameters(1)
    init_param[0] = [0.0]
    rates:vec = [0.0]
    
    tanh_layer = DeepLearningLayer(name:="tanh",forward_pass:=deeplearning_tanh_forward,backward_pass:=deeplearning_tanh_backward,parameters:=init_param,shrink_rates:=rates)
endfunction

% Function: deeplearning_RELU_forward
% Internal function for the processing performed by a rectified linear unit (ReLU) activation layer. Please refer to <get_RELU_layer> instead.
%
% See also:
%    <get_RELU_layer>
%
function [output:Data] = deeplearning_RELU_forward(input:Data,parameters:Parameters)
    output = max(input,0)
endfunction

% Function: deeplearning_RELU_backward
% Internal function for the processing performed by a rectified linear unit (ReLU) activation layer. Please refer to <get_RELU_layer> instead.
%
% See also:
%    <get_RELU_layer>
%
function [dzdinput:Data,dzdparameters:Parameters] = deeplearning_RELU_backward(input:Data,output:Data,parameters:Parameters,dzdoutput:Data)
    dzdinput = dzdoutput.*(input>0)
    dzdparameters = Parameters(1)
    dzdparameters[0] = [0.0]
endfunction

% Function: get_RELU_layer
% Function for acquiring a pre-constructed rectified linear unit (ReLU) activation layer.
% To be used in conjunction with <add_layer>.
%
%   function [RELU_layer:^DeepLearningLayer] = get_RELU_layer()
%
% See also:
%    <add_layer>
%
function [RELU_layer:^DeepLearningLayer] = get_RELU_layer()
    init_param = Parameters(1)
    init_param[0] = [0.0]
    rates:vec = [0.0]
    
    RELU_layer = DeepLearningLayer(name:="ReLU",forward_pass:=deeplearning_RELU_forward,backward_pass:=deeplearning_RELU_backward,parameters:=init_param,shrink_rates:=rates)
endfunction 

% Function: deeplearning_leaky_RELU_forward
% Internal function for the processing performed by a leaky rectified linear unit (ReLU) activation layer. Please refer to <get_leaky_RELU_layer> instead.
%
% See also:
%    <get_leaky_RELU_layer>
%
function [output:Data] = deeplearning_leaky_RELU_forward(input:Data,parameters:Parameters)
    leakage = parameters[0][0]
    output = max(input,input*leakage)
endfunction

% Function: deeplearning_leaky_RELU_backward
% Internal function for the processing performed by a leaky rectified linear unit (ReLU) activation layer. Please refer to <get_leaky_RELU_layer> instead.
%
% See also:
%    <get_leaky_RELU_layer>
%
function [dzdinput:Data,dzdparameters:Parameters] = deeplearning_leaky_RELU_backward(input:Data,output:Data,parameters:Parameters,dzdoutput:Data)
    leakage = parameters[0][0]
    dzdinput = dzdoutput.*((input>0)*(1-leakage)+leakage)
    dzdparameters = Parameters(1)
    dzdparameters[0] = [0.0]
endfunction

% Function: get_leaky_RELU_layer
% Function for acquiring a pre-constructed leaky rectified linear unit (ReLU) activation layer.
% This leaky ReLU will not train its leakage parameter - it is set statically.
% To be used in conjunction with <add_layer>.
%
%   function [leaky_RELU_layer:^DeepLearningLayer] = get_leaky_RELU_layer()
%
% See also:
%    <add_layer>
%
function [leaky_RELU_layer:^DeepLearningLayer] = get_leaky_RELU_layer(leakage:scalar=0.0)
    init_param = Parameters(1)
    init_param[0] = [leakage]
    rates:vec = [0.0]
    
    leaky_RELU_layer = DeepLearningLayer(name:="leaky ReLU",forward_pass:=deeplearning_leaky_RELU_forward,backward_pass:=deeplearning_leaky_RELU_backward,parameters:=init_param,shrink_rates:=rates)
endfunction 

% Function: initDeepLearningNetwork
% Function for acquiring a correctly initialized DeepLearningNetwork.
% The maximum number of layers the network can have is (currently) fixed at initialization time, and handed as an optional parameter.
% After initialization, the loss function still needs to be set with <set_loss_function>, and layers need to be added with <add_layer>.
%
%   function [network:DeepLearningNetwork] = initDeepLearningNetwork(max_layers:int=20)
%
% See also:
%    <add_layer>,<set_loss_function>,<train>,<process>,<save_to_file>,<load_from_file>
%
function [network:DeepLearningNetwork] = initDeepLearningNetwork(max_layers:int=20)
    default_settings = DeepLearningSettings(rate:=1e-3,momentum:= 0.85,plot_period:= 10)
    network = DeepLearningNetwork(loss:=deeplearning_empty_loss,loss_name:="empty",layers:=DeepLearningLayers(max_layers),settings:=default_settings,nr_layers:=0)
endfunction

% Function: set_loss_function
% Function for setting the loss function used in an initialized DeepLearningNetwork.
% Currently, alternatives are either binary classification ("binary") or MSE to the groundtruth ("MSE")
%
%   function [] = set_loss_function(self:^DeepLearningNetwork,name:string)
%
% See also:
%    <initDeepLearningNetwork>
%
function [] = set_loss_function(self:^DeepLearningNetwork,name:string)
    self.loss_name = name
    match name with
    | "MSE" -> loss_info = deeplearning_MSE_loss
    | "MAE" -> loss_info = deeplearning_MAE_loss
    | "binary" -> loss_info = deeplearning_binary_loss
    |_ -> error(sprintf("The loss function %s is currently not implemented"),name)
    endmatch
    self.loss = loss_info
endfunction

% Function: add_layer
% Function for adding a new layer to an initialized DeepLearningNetwork.
%
%   function [] = add_layer(self:^DeepLearningNetwork,layer:^DeepLearningLayer)
%
% Currently, the following layers are implemented:
%   - <get_dilation_layer>
%   - <get_erosion_layer>
%   - <get_convolve_layer>
%   - <get_fully_connected_layer>
%   - <get_subsample_NN_layer>
%   - <get_upsample_NN_layer>
%   - <get_sigmoid_layer>
%   - <get_tanh_layer>
%   - <get_RELU_layer>
%
% See also:
%    <initDeepLearningNetwork>
%
function [] = add_layer(self:^DeepLearningNetwork,layer:^DeepLearningLayer)
    self.layers[self.nr_layers] = layer
    self.nr_layers = self.nr_layers + 1
endfunction
