package org.latikai.vici.datatypes;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import org.jtransforms.fft.DoubleFFT_1D;
import org.jtransforms.dct.DoubleDCT_1D;
import org.latikai.vici.modules.AudioCapture;

public class AudioSample {

    protected static DoubleFFT_1D fft_bot;
    protected static DoubleDCT_1D dct_bot;
    protected static final int mel_scales;
    public static final int cepstra_length = 13;

    static {
        //how many mel scales are fully contained in this spectrum?
        int sample_count = AudioCapture.batchSize * 3 / 2;
        double max_frequency = indexToHertz(sample_count - 1);
        double max_mel = hertzToMel(max_frequency);
        mel_scales = (int) Math.floor(max_mel / 100) - 1;
        //initialize the fft and dct calculators
        fft_bot = new DoubleFFT_1D(sample_count);
        dct_bot = new DoubleDCT_1D(mel_scales);
    }

    protected double[] melcepstra;

    public AudioSample(byte[] samples1, byte[] samples2, byte[] samples3) {
        double[] fftdata, powerspectrum, melspectrum, logmelspectrum;
        int sample_count = (samples1.length + samples2.length + samples3.length) / 2;
        fftdata = new double[sample_count * 2];
        //merge all of the samples into the single array
        ShortBuffer sb = ByteBuffer.wrap(samples1).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
        for (int i = 0; i < samples1.length / 2; i++) {
            fftdata[i] = sb.get() / 32768.0f;
        }
        sb = ByteBuffer.wrap(samples2).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
        for (int i = 0; i < samples2.length / 2; i++) {
            fftdata[samples1.length / 2 + i] = sb.get() / 32768.0f;
        }
        sb = ByteBuffer.wrap(samples3).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer();
        for (int i = 0; i < samples3.length / 2; i++) {
            fftdata[samples1.length / 2 + samples2.length / 2 + i] = sb.get() / 32768.0f;
        }

        //step 1: pre-emphasis
        // high-pass filter to model the cochlea
        for (int i = fftdata.length - 1; i > 0; i--) {
            fftdata[i] = fftdata[i] - 0.95f * fftdata[i - 1];
        }

        //step 2: windowing
        // use a windowing function (currently a hamming window)
        for (int i = 0; i < fftdata.length; i++) {
            double coefficient = 0.54f - 0.46f * Math.cos((2 * Math.PI * i) / fftdata.length);
            fftdata[i] = fftdata[i] * coefficient;
        }

        //step 3: spectral analysis
        fft_bot.realForwardFull(fftdata);

        powerspectrum = new double[sample_count];
        //step 3.b: estimating the power spectrum of the signal
        for (int i = 0; i < powerspectrum.length; i++) {
            powerspectrum[i] = fftdata[2 * i] * fftdata[2 * i] + fftdata[2 * i + 1] * fftdata[2 * i + 1];
        }

        melspectrum = new double[mel_scales];
        //step 4: mel-spectrum
        for (int mel = 0; mel < melspectrum.length; mel++) {
            int previousIndex = hertzToIndex(melToHertz(100 * mel));
            int thisIndex = hertzToIndex(melToHertz(100 * (mel + 1)));
            int nextIndex = hertzToIndex(melToHertz(100 * (mel + 2)));

            double sum = 0.0;
            for (int i = previousIndex; i < thisIndex; i++) {
                double coefficient = ((double) i - previousIndex) / (thisIndex - previousIndex);
                sum += coefficient * powerspectrum[i];
            }
            for (int i = thisIndex; i < nextIndex; i++) {
                double coefficient = ((double) nextIndex - i) / (nextIndex - thisIndex);
                sum += coefficient * powerspectrum[i];
            }

            melspectrum[mel] = sum;
        }

        //step 5: dynamic compression (log-mel scale)
        logmelspectrum = new double[sample_count];
        for (int i = 0; i < melspectrum.length; i++) {
            logmelspectrum[i] = Math.log(melspectrum[i] + 0.1);
        }

        //step 6: mel cepstra
        dct_bot.forward(logmelspectrum, true);
        melcepstra = new double[cepstra_length];
        for (int i = 0; i < melcepstra.length; i++) {
            melcepstra[i] = logmelspectrum[i + 1];
        }
    }

    private static double indexToHertz(int index) {
        return (index * AudioCapture.sampleRate) / (AudioCapture.batchSize * 3);
    }

    private static int hertzToIndex(double hertz) {
        return ((int) Math.round((hertz * AudioCapture.batchSize * 3) / AudioCapture.sampleRate));
    }

    private static double melToHertz(double mel) {
        return 700 * (Math.pow(10, mel / 2595) - 1);
    }

    private static double hertzToMel(double hertz) {
        return 2595 * Math.log10(1 + hertz / 700);
    }

    public double[] getMelCepstra() {
        return melcepstra;
    }
}
