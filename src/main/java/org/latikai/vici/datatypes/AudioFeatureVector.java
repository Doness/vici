package org.latikai.vici.datatypes;

import java.io.Serializable;

public class AudioFeatureVector implements Serializable {

    private static final long serialVersionUID = 1L;

    protected double[] vector;

    public AudioFeatureVector(double[] vector) {
        this.vector = vector;
    }

    public double[] getValues() {
        return vector;
    }

    public double getEnergy() {
        double energy = 0.0;
        for (int i = 0; i < vector.length; i++) {
            energy += vector[i] * vector[i];
        }
        return energy;
    }
    
    public double getSimilarity(AudioFeatureVector that)
    {
        double score = 0.0, scale0 = 0.0, scale1 = 0.0;
        for (int i = 0; i < this.vector.length; i++) {
            double val0 = this.vector[i];
            double val1 = that.vector[i];
            double val2 = (this.vector[i] - that.vector[i]);
            score  += val2*val2;
            scale1 += val1*val1;
            scale0 += val0*val0;
        }
        return score/Math.sqrt(scale0)/Math.sqrt(scale1);
    }
}