package org.latikai.vici.modules;

import org.latikai.vici.datatypes.AudioSample;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import org.latikai.utils.CircularBuffer;
import org.latikai.utils.LatikaiLogging;
import org.latikai.vici.datatypes.AudioFeatureVector;

public class AudioCapture extends ViciModule {

    protected static class SampleBuffer extends CircularBuffer<AudioSample> {

        SampleBuffer() {
            super(9);
            super.setOffset(-4);
        }

        boolean hasFeatureVector() {
            return super.full();
        }

        AudioFeatureVector getFeatureVector() {
            if (!hasFeatureVector()) {
                return null;
            }

            int d = AudioSample.cepstra_length;
            double[] vector = new double[3 * d];

            for (int i = 0; i < d; i++) {
                //original data
                vector[i] = this.get(0).getMelCepstra()[i];
                //first order derivatives
                double[] ddt = new double[5];
                for (int j = 0; j < ddt.length; j++) {
                    int jj = j - 2;
                    ddt[j] = (2 * (this.get(jj + 2).getMelCepstra()[i] - this.get(jj - 2).getMelCepstra()[i]) + (this.get(jj + 1).getMelCepstra()[i] - this.get(jj - 1).getMelCepstra()[i])) / 10;
                }
                vector[d + i] = ddt[2];
                //second order derivative
                vector[2 * d + i] = (2 * (ddt[0] - ddt[4]) + (ddt[1] - ddt[3])) / 10;
            }

            return new AudioFeatureVector(vector);
        }
    }

    protected final MainFrame parent;
    public final static float sampleRate = 16000.0F;
    public final static float batchRate = 100.0F;
    public final static int batchSize = (int) (sampleRate / batchRate);
    public final static boolean bigEndian = false;
    protected byte[] buffer;
    protected final CircularBuffer<byte[]> window_buffer;
    protected final SampleBuffer sample_buffer;

    public AudioCapture(MainFrame parent) {
        this.parent = parent;
        this.window_buffer = new CircularBuffer<>(3);
        this.sample_buffer = new SampleBuffer();
        this.buffer = new byte[batchSize];
    }

    protected AudioFormat getAudioFormat() {
        //this means the byte buffer will have to be wrapped in a ShortBuffer
        int sampleSizeInBits = 16;
        int channels = 1;
        boolean signed = true;
        return new AudioFormat(
                sampleRate,
                sampleSizeInBits,
                channels,
                signed,
                bigEndian);
    }

    protected void processBatch() {
        byte[] previous_buffer = this.buffer;
        this.buffer = new byte[batchSize];
        window_buffer.add(previous_buffer);
        if (window_buffer.full()) {
            sample_buffer.add(new AudioSample(window_buffer.get(-2), window_buffer.get(-1), window_buffer.get(0)));
            if (sample_buffer.hasFeatureVector()) {
                AudioFeatureVector fv = sample_buffer.getFeatureVector();
                //what should we actually do with this feature vector?
            }
        }
    }

    @Override
    public void run() {
        TargetDataLine tdl;
        AudioFormat format = getAudioFormat();
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        if (!AudioSystem.isLineSupported(info)) {
            LatikaiLogging.error("Line not supported!");
        }

        try {
            tdl = (TargetDataLine) AudioSystem.getLine(info);
            tdl.open(format);
            tdl.start();
            while (running && tdl.isOpen()) {
                tdl.read(buffer, 0, batchSize);
                processBatch();
            }
            tdl.stop();
        } catch (LineUnavailableException ex) {
            LatikaiLogging.error("Line unavailable!");
        }
    }
}
