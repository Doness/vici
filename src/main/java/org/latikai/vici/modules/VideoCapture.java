/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.latikai.vici.modules;

import org.bytedeco.javacv.*;
import javax.swing.JFrame;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import org.bytedeco.javacpp.*;
import org.bytedeco.javacv.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 *
 * @author sdonn
 */
public class VideoCapture extends ViciModule {
    protected final MainFrame parent;
    
    public VideoCapture(MainFrame parent) {
        this.parent = parent;
    }
    
    @Override
    public void run() {
        System.out.println("Video capture started");
        CanvasFrame frame = new CanvasFrame("Web Cam");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
        
        IplImage this_frame_rgb = null, this_frame_gray = null,this_frame = null, last_frame= null, this_diff = null, last_diff = null;
        
        try
        {
            grabber.start();
            while (running)
            {
                if(this_frame != null)
                    last_frame = this_frame.clone();
                this_frame_rgb = converter.convert(grabber.grab());
                this_frame = IplImage.create(this_frame_rgb.cvSize(), this_frame_rgb.depth(), 1);
                cvCvtColor(this_frame_rgb, this_frame, CV_RGB2GRAY);
                cvFlip(this_frame,this_frame,1);
                
                if (last_frame != null)
                {
                    
                    last_diff = this_diff;
                    cvCvtScale(last_diff, last_diff, 0.5, 0);
                    this_diff = IplImage.create(this_frame.cvSize(), this_frame.depth(), this_frame.nChannels());
                    cvAbsDiff(this_frame,last_frame,this_diff);
                    cvThreshold(this_diff,this_diff,64,255,CV_THRESH_BINARY);
                    
                    if(last_diff != null)
                    {
                        IplImage diff2 = IplImage.create(this_frame.cvSize(), this_frame.depth(), this_frame.nChannels());
                        cvCvtScale(this_diff, diff2, 0.5, 128);
                        cvSub(diff2,last_diff,diff2);
                        frame.showImage(converter.convert(diff2));
                    }
                }
            }
            grabber.stop();
            frame.dispose();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
