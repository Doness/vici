package org.latikai.vici.modules;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.latikai.utils.LatikaiLogging;

public class CommandProcessor extends ViciModule {

    protected MainFrame parent;
    protected BufferedImage logo;

    public CommandProcessor(MainFrame parent) {
        this.parent = parent;
        try {
            this.logo = ImageIO.read(getClass().getResourceAsStream("/logo/VICI.png"));
        } catch (IOException ex) {
        }
    }

    protected void processCommand(String command) {
        if(command.contains("VICI"))
        {
                parent.say("You have my attention.");
//                Runtime.getRuntime().exec("firefox");
        }
    }

    @Override
    public void run() {
        while (running) {
            synchronized (parent.cb) {
                while (parent.cb.isEmpty() && running) {
                    try {
                        parent.cb.wait(1000);
                    } catch (InterruptedException ex) {
                        LatikaiLogging.debug("CommandCompressor exiting because of an interrupt.");
                        return;
                    }
                }
                if (running && !parent.cb.isEmpty()) {
                    String command = parent.cb.remove(parent.cb.size() - 1);
                    processCommand(command);
                }
            }
        }
    }
}
