package org.latikai.vici.modules;

public abstract class ViciModule implements Runnable {

    protected boolean running = true;

    public void shutDown() {
        running = false;
    }
}
