package org.latikai.vici.modules;

import org.latikai.utils.LatikaiLogging;
import org.latikai.vici.datatypes.CommandBuffer;

public class MainFrame extends ViciModule {

    protected static MainFrame instance;
    protected final CommandBuffer cb;

    protected MainFrame() {
        cb = new CommandBuffer();
        initializeAudioCapture();
        initializeCommandProcessor();
        say("Vicky on-line.");
    }

    protected AudioCapture ac;
    protected CommandProcessor cp;

    protected void initializeAudioCapture() {
        ac = new AudioCapture(this);
    }
    
    protected void initializeCommandProcessor() {
        cp = new CommandProcessor(this);
    }

    protected void say(String value) {
        System.out.println("[VICI] "+value);
    }

    protected void dispose() {
        running = false;
    }

    @Override
    public void run() {
        Thread act = new Thread(ac, "Audio Capture");
        act.start();

        Thread cpt = new Thread(cp, "Command Processor");
        cpt.start();

        while (running) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                break;
            }
        }

        ac.shutDown();
        cp.shutDown();

        dispose();

        try {
            cpt.join();
            act.join();
        } catch (InterruptedException ex) {
        }
    }

    public static MainFrame getInstance() {
        if (instance == null) {
            instance = new MainFrame();
        }
        return instance;
    }

    public void queueCommand(String command) {
        synchronized (cb) {
            cb.add(command);
        }
    }

    public static void main(String[] args) throws Exception {
        LatikaiLogging.initializeLogging("VICI");
        LatikaiLogging.enableDebugging();
        MainFrame mf = getInstance();
        Thread mft = new Thread(mf);
        mft.start();
        mft.join();
    }
}
