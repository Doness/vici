package org.latikai.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Circular buffer implementation. It only allows non-positive indexing up to
 * the array size. The implementation is thread-safe and can be used from
 * multiple threads.
 *
 * @author Simon Donné
 */
public class CircularBuffer<T> {

    /**
     * Data array.
     */
    protected final T[] data;

    /**
     * Pointer to the last-added element.
     */
    protected int index;

    /**
     * Indicates how many valid entries this Circular buffer contains.
     */
    protected int elements;

    /**
     * Offset when indexing the buffer.
     */
    protected int offset = 0;

    /**
     * Constructs an empty CircularBuffer with the specified capacity.
     *
     * @param capacity the capacity of the circular buffer
     * @throws IllegalArgumentException if the specified capacity is negative
     */
    public CircularBuffer(int capacity) {
        data = (T[]) new Object[capacity];
        index = data.length - 1;
        elements = 0;
    }

    /**
     * Adds the argument to this buffer, and pushes present data back in
     * history.
     *
     * @param value the element to be added
     */
    public synchronized void add(T value) {
        index = (index + 1) % data.length;
        data[index] = value;
        elements = Math.max(elements, index + 1);
    }

    /**
     * Sets the buffer's offset for indexing.
     *
     * @param offset the new offset to use
     */
    public synchronized void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * Get the specified element from this buffer's history. The buffer can only
     * be indexed with non-positive indices, up to the number of valid elements
     * in this buffer.
     *
     * @param i the location of the required element in history
     * @throws ArrayIndexOutOfBoundsException if the specified index does not
     * contain a valid entry.
     */
    public synchronized T get(int i) {
        i = i + offset;
        if (i > 0) {
            throw new ArrayIndexOutOfBoundsException("You can only look in this buffer's history.");
        }
        if (i <= -elements) {
            throw new ArrayIndexOutOfBoundsException("The circular buffer is smaller than this.");
        }
        return data[(index + i + data.length) % data.length];
    }

    /**
     * Get the capacity of this circular buffer.
     *
     * @return the capacity of this buffer.
     */
    public int getCapacity() {
        return data.length;
    }

    /**
     * Check whether this circular buffer is full of valid elements
     *
     * @return whether this buffer is completely filled.
     */
    public synchronized boolean full() {
        return (elements == data.length);
    }
    
    /**
     * Get the ordered list of all elements.
     * 
     * @return A List with all of the elements in their respective order.
     */
    public synchronized List<T> asList() {
        ArrayList<T> list = new ArrayList();
        for(int i = 1-this.elements; i <= 0; i++)
        {
            list.add(this.get(i));
        }
        return list;
    }
}
