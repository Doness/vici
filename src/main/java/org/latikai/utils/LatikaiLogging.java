package org.latikai.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public final class LatikaiLogging {
    private static boolean debug = false;
    
    public enum MessageType {

        DEBUG, WARNING, ERROR, INFO, SILENT
    };

    private static boolean initialized = false;
    private static BufferedWriter output_file;

    public static void enableDebugging()
    {
        debug = true;
    }
    
    public static void initializeLogging(String name) {
        try {
            output_file = new BufferedWriter(new FileWriter(name + ".log"));
        } catch (IOException e) {
            System.out.println("Error initializing the logging framework!");
            output_file = null;
        }
        initialized = true;
    }

    public static void message(String message, MessageType type) {
        if(type == MessageType.DEBUG && !debug)
        {
            return;
        }
        
        if (!initialized) {
            System.out.println("\u001B[31m" + "Logging framework not initialized properly! Only console output available." + "\u001B[0m");
            initialized = true;
        }

        if (output_file != null) {
            try {
                output_file.write("[" + type.toString() + "] " + message + "\n");
                output_file.flush();
            } catch (IOException e) {
                System.out.println("Error writing to the log file! Log file closed.");
                output_file = null;
            }
        }

        String color_code = "";
        switch (type) {
            case DEBUG:
//                color_code = "\u001B[34m";//blue
                break;
            case INFO:
                color_code = "\u001B[30m";//black
                break;
            case WARNING:
                color_code = "\u001B[36m";//orange
                break;
            case ERROR:
                color_code = "\u001B[31m";//red
                break;
            case SILENT:
                return;
        }

        System.out.println(color_code + message + "\u001B[0m");
        System.out.flush();
    }

    public static void error(String message) {
        message(message, MessageType.ERROR);
    }

    public static void warning(String message) {
        message(message, MessageType.WARNING);
    }

    public static void debug(String message) {
        message(message, MessageType.DEBUG);
    }

    public static void silent(String message) {
        message(message, MessageType.SILENT);
    }

    public static void info(String message) {
        message(message, MessageType.INFO);
    }
}
